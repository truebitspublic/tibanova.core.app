SET FOREIGN_KEY_CHECKS=0;
DELETE FROM tbl_permission_block;
DELETE FROM tbl_permission_group;
DELETE FROM tbl_permission;
SET FOREIGN_KEY_CHECKS=1;

#2016/11/03 --
################

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (1, 'Default', 'Default', 'jeremi16us', '2016-11-02 00:00:00', 0, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (1, 'Default Permissions', 'Default Permissions', 1, 'jeremi16us', '2016-11-02 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1, 'ROLE_ADMIN', 'All Permission', 1, 'jeremi16us', '2016-11-02 00:00:00', 1, 1),
(2, 'ROLE_USER', 'Default Permission', 1, 'jeremi16us', '2016-11-02 00:00:00', 1, 2);

#2016/11/17 --
################

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (2, 'Setup & Configuration', 'Setup & Configuration', 'jeremi16us', '2016-11-17 00:00:00', 1, 1);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (3, 'Reception Works', 'Reception Works', 'jeremi16us', '2016-02-06 00:00:00', 1, 2);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (4, 'Radiology Works', 'Radiology Works', 'jeremi16us', '2016-12-07 00:00:00', 1, 3);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (5, 'Laboratory Works', 'Laboratory Works', 'jeremi16us', '2016-12-13 00:00:00', 1, 4);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (6, 'Store Works', 'Store Works', 'jeremi16us', '2016-12-14 00:00:00', 1, 5);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (7, 'Procurement Works', 'Procurement Works', 'jeremi16us', '2017-01-20 00:00:00', 1, 6);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (8, 'Pharmacy Works', 'Pharmacy Works', 'jeremi16us', '2017-02-21 00:00:00', 1, 7);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (9, 'Revenue Cycle', 'Revenue Cycle', 'jeremi16us', '2017-04-12 00:00:00', 1, 3);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (10, 'Nurse', 'Nurse', 'jeremi16us', '2017-06-28 00:00:00', 1, 2);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (11, 'Doctor\'s Works', 'Doctor\'s Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 2);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (12, 'Dialysis Works', 'Dialysis Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 12);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (13, 'General Ledger Works', 'General Ledger Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 13);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (14, 'Surgery Works', 'Surgery Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 14);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (15, 'Procedure Works', 'Procedure Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 15);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (16, 'Patient Records', 'Patient Records', 'jeremi16us', '2017-06-28 00:00:00', 1, 16);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (17, 'Management', 'Management', 'jeremi16us', '2017-06-28 00:00:00', 1, 17);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (18, 'Quality Assurance Works', 'Quality Assurance Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 18);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (19, 'Patient Billing Works', 'Patient Billing Works', 'jeremi16us', '2017-06-28 00:00:00', 1, 19);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (20, 'Optical', 'Optical', 'jeremi16us', '2017-06-28 00:00:00', 1, 20);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (21, 'DHIS REPORT', 'DHIS REPORT', 'jeremi16us', '2017-06-28 00:00:00', 1, 21);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (22, 'Directory', 'Directory', 'jeremi16us', '2017-06-28 00:00:00', 1, 22);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (23, 'Employee Type', 'Employee Type', 'jeremi16us', '2018-01-09 00:00:00', 1, 23);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (24, 'Debtors Management', 'Debtors Management', 'jeremi16us', '2019-03-18 00:00:00', 1, 24);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (25, 'Claim Processing', 'Claim Processingt', 'jeremi16us', '2019-04-29 00:00:00', 1, 25);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (26, 'Message', 'Message', 'jeremi16us', '2019-04-29 00:00:00', 1, 26);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (27, 'Home care', 'Home care', 'jeremi16us', '2021-05-15 00:00:00', 1, 27);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (28, 'Document Management', 'Document Management', 'jeremi16us', '2021-05-15 00:00:00', 1, 28);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (29, 'Accounting', 'Accounting', 'jeremi16us', '2023-03-02 00:00:00', 1, 29);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (30, 'Appointment Booking', 'Appointment Booking', 'josephKiwia', '2023-03-02 00:00:00', 1, 30);


INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (31, 'Human Resource', 'Human Resource', 'josephKiwia', '2023-11-05 00:00:00', 1, 31);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (32, 'Asset Management', 'Asset Management', 'josephKiwia', '2023-11-05 00:00:00', 1, 32);

INSERT INTO tbl_permission_block (id, name, description, created_by, created_time, active, sort)
VALUES (33, 'Bulksms Module', 'Bulksms Module', 'festus', '2023-11-05 00:00:00', 1, 33);


INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (2, 'Company', 'Company', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (11, 'ROLE_COMPANY_VIEW', 'VIEW', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(12, 'ROLE_COMPANY_CREATE', 'CREATE', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(13, 'ROLE_COMPANY_EDIT', 'EDIT', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(14, 'ROLE_COMPANY_DELETE', 'DELETE', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (3, 'Branch', 'Branch', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (21, 'ROLE_BRANCH_VIEW', 'VIEW', 3, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(22, 'ROLE_BRANCH_CREATE', 'CREATE', 3, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(23, 'ROLE_BRANCH_EDIT', 'EDIT', 3, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(24, 'ROLE_BRANCH_DELETE', 'DELETE', 3, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(25, 'ROLE_ASSIGN_BRANCH_TO_EMPLOYEE', 'ASSIGN BRANCH TO EMPLOYEE', 3, 'jeremi16us', '2016-11-17 00:00:00', 1, 0);



INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (4, 'Sponsor', 'Sponsor', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 4);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (31, 'ROLE_SPONSOR_VIEW', 'VIEW', 4, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(32, 'ROLE_SPONSOR_CREATE', 'CREATE', 4, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(33, 'ROLE_SPONSOR_EDIT', 'EDIT', 4, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(34, 'ROLE_SPONSOR_DELETE', 'DELETE', 4, 'jeremi16us', '2016-11-17 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (5, 'User Role', 'User Role', 2, 'jeremi16us', '2016-11-17 00:00:00', 1, 5);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (41, 'ROLE_USER_ROLE_VIEW', 'VIEW', 5, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(42, 'ROLE_USER_ROLE_CREATE', 'CREATE', 5, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(43, 'ROLE_USER_ROLE_EDIT', 'EDIT', 5, 'jeremi16us', '2016-11-17 00:00:00', 1, 0),
(44, 'ROLE_USER_ROLE_DELETE', 'DELETE', 5, 'jeremi16us', '2016-11-17 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (6, 'Employee', 'Employee', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 6);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (51, 'ROLE_EMPLOYEE_VIEW', 'VIEW', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(52, 'ROLE_EMPLOYEE_CREATE', 'CREATE', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(53, 'ROLE_EMPLOYEE_EDIT', 'EDIT', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(54, 'ROLE_EMPLOYEE_DELETE', 'DELETE', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(55, 'ROLE_ASSIGN_EMPLOYEE_TO_BRANCH', 'ASSIGN EMPLOYEE TO BRANCH', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(56, 'ROLE_ASSIGN_EMPLOYEE_TO_SUB_DEPARTMENT', 'ASSIGN EMPLOYEE TO SUB DEPARTMENT', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(57, 'ROLE_ASSIGN_EMPLOYEE_TO_CLINIC', 'ASSIGN EMPLOYEE TO CLINIC', 6, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(58, 'ROLE_ASSIGN_EMPLOYEE_TO_USER_ROLE', 'ASSIGN EMPLOYEE TO USER ROLE', 6, 'jeremi16us', '2018-04-25 00:00:00', 1, 0);


INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (7, 'Clinic', 'Clinic', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 7);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (61, 'ROLE_CLINIC_VIEW', 'VIEW', 7, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(62, 'ROLE_CLINIC_CREATE', 'CREATE', 7, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(63, 'ROLE_CLINIC_EDIT', 'EDIT', 7, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(64, 'ROLE_CLINIC_DELETE', 'DELETE', 7, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(65, 'ROLE_ASSIGN_CLINIC_TO_EMPLOYEE', 'ASSIGN CLINIC TO EMPLOYEE', 7, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (8, 'Department', 'Department', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (71, 'ROLE_DEPARTMENT_VIEW', 'VIEW', 8, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(72, 'ROLE_DEPARTMENT_CREATE', 'CREATE', 8, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(73, 'ROLE_DEPARTMENT_EDIT', 'EDIT', 8, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(74, 'ROLE_DEPARTMENT_DELETE', 'DELETE', 8, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(75, 'ROLE_ASSIGN_DEPARTMENT_TO_EMPLOYEE', 'ASSIGN DEPARTMENT TO EMPLOYEE', 8, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (9, 'Sub Department', 'Sub Department', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 9);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (81, 'ROLE_SUB_DEPARTMENT_VIEW', 'VIEW', 9, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(82, 'ROLE_SUB_DEPARTMENT_CREATE', 'CREATE', 9, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(83, 'ROLE_SUB_DEPARTMENT_EDIT', 'EDIT', 9, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(84, 'ROLE_SUB_DEPARTMENT_DELETE', 'DELETE', 9, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(85, 'ROLE_ASSIGN_SUB_DEPARTMENT_TO_EMPLOYEE', 'ASSIGN SUB DEPARTMENT TO EMPLOYEE', 9, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (10, 'Setup & Configuration', 'Setup & Configuration', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (91, 'ROLE_SETUP_AND_CONFIGURATION_VIEW', 'VIEW', 10, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
        (92, 'ROLE_SETUP_AND_CONFIGURATION_ALLOW_LOGIN_AS', 'ALLOW LOGIN AS', 10, 'jeremi16us', '2021-05-22 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (11, 'Discharge Reason', 'Discharge Reason', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 10);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (101, 'ROLE_DISCHARGE_REASON_VIEW', 'VIEW', 11, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(102, 'ROLE_DISCHARGE_REASON_CREATE', 'CREATE', 11, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(103, 'ROLE_DISCHARGE_REASON_EDIT', 'EDIT', 11, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(104, 'ROLE_DISCHARGE_REASON_DELETE', 'DELETE', 11, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (12, 'Ward', 'Ward', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 11);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (111, 'ROLE_HOSPITAL_WARD_VIEW', 'VIEW', 12, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(112, 'ROLE_HOSPITAL_WARD_CREATE', 'CREATE', 12, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(113, 'ROLE_HOSPITAL_WARD_EDIT', 'EDIT', 12, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(114, 'ROLE_HOSPITAL_WARD_DELETE', 'DELETE', 12, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (13, 'Country', 'Country', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 12);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (121, 'ROLE_COUNTRY_VIEW', 'VIEW', 13, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(122, 'ROLE_COUNTRY_CREATE', 'CREATE', 13, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(123, 'ROLE_COUNTRY_EDIT', 'EDIT', 13, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(124, 'ROLE_COUNTRY_DELETE', 'DELETE', 13, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (14, 'Region', 'Region', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 13);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (131, 'ROLE_REGION_VIEW', 'VIEW', 14, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(132, 'ROLE_REGION_CREATE', 'CREATE', 14, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(133, 'ROLE_REGION_EDIT', 'EDIT', 14, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(134, 'ROLE_REGION_DELETE', 'DELETE', 14, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(135, 'ROLE_REGION_SET_DEFAULT', 'SET DEFAULT REGION', 14, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (15, 'District', 'District', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 14);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (141, 'ROLE_DISTRICT_VIEW', 'VIEW', 15, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(142, 'ROLE_DISTRICT_CREATE', 'CREATE', 15, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(143, 'ROLE_DISTRICT_EDIT', 'EDIT', 15, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(144, 'ROLE_DISTRICT_DELETE', 'DELETE', 15, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (16, 'Supplier', 'Supplier', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 15);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (151, 'ROLE_SUPPLIER_VIEW', 'VIEW', 16, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(152, 'ROLE_SUPPLIER_CREATE', 'CREATE', 16, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(153, 'ROLE_SUPPLIER_EDIT', 'EDIT', 16, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(154, 'ROLE_SUPPLIER_DELETE', 'DELETE', 16, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (17, 'Currency', 'Currency', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 16);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (161, 'ROLE_CURRENCY_VIEW', 'VIEW', 17, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(162, 'ROLE_CURRENCY_CREATE', 'CREATE', 17, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(163, 'ROLE_CURRENCY_EDIT', 'EDIT', 17, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(164, 'ROLE_CURRENCY_DELETE', 'DELETE', 17, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (18, 'Vital', 'Vital', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 17);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (171, 'ROLE_VITAL_VIEW', 'VIEW', 18, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(172, 'ROLE_VITAL_CREATE', 'CREATE', 18, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(173, 'ROLE_VITAL_EDIT', 'EDIT', 18, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(174, 'ROLE_VITAL_DELETE', 'DELETE', 18, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (19, 'Referral Hospital', 'Referral Hospital', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 18);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (181, 'ROLE_REFERRAL_HOSPITAL_VIEW', 'VIEW', 19, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(182, 'ROLE_REFERRAL_HOSPITAL_CREATE', 'CREATE', 19, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(183, 'ROLE_REFERRAL_HOSPITAL_EDIT', 'EDIT', 19, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(184, 'ROLE_REFERRAL_HOSPITAL_DELETE', 'DELETE', 19, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(185, 'ROLE_REFERRAL_HOSPITAL_CAN_APPROVE', 'APPROVE', 19, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(186, 'ROLE_REFERRAL_HOSPITAL_CAN_PRINT', 'PRINT', 19, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (20, 'Course Of Injury', 'Course Of Injury', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 19);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (191, 'ROLE_COURSE_OF_INJURY_VIEW', 'VIEW', 20, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(192, 'ROLE_COURSE_OF_INJURY_CREATE', 'CREATE', 20, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(193, 'ROLE_COURSE_OF_INJURY_EDIT', 'EDIT', 20, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(194, 'ROLE_COURSE_OF_INJURY_DELETE', 'DELETE', 20, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (21, 'Email Recepient', 'Email Recepient', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 20);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (201, 'ROLE_EMAIL_RECEPIENT_VIEW', 'VIEW', 21, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(202, 'ROLE_EMAIL_RECEPIENT_CREATE', 'CREATE', 21, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(203, 'ROLE_EMAIL_RECEPIENT_EDIT', 'EDIT', 21, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(204, 'ROLE_EMAIL_RECEPIENT_DELETE', 'DELETE', 21, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (22, 'Procument Configuration', 'Procument Configuration', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 21);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (211, 'ROLE_CONFIGURE_APPROVAL_LEVEL', 'CONFIGURE APPROVAL LEVEL', 22, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(212, 'ROLE_ASSIGN_EMPLOYEE_APPROVAL_LEVEL', 'ASSIGN EMPLOYEE APPROVAL LEVEL', 22, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(213, 'ROLE_STOCK_MOVEMENT_CONFIGURATION', 'STOCK MOVEMENT CONFIGURATION', 22, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (23, 'Audit Log', 'Audit Log', 2, 'jeremi16us', '2016-11-22 00:00:00', 1, 0);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (221, 'ROLE_AUDIT_LOG_VIEW', 'VIEW', 23, 'jeremi16us', '2016-11-22 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (24, 'Items Category', 'Items Category', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 22);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (231, 'ROLE_ITEMS_CATEGORY_VIEW', 'VIEW', 24, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(232, 'ROLE_ITEMS_CATEGORY_CREATE', 'CREATE', 24, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(233, 'ROLE_ITEMS_CATEGORY_EDIT', 'EDIT', 24, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(234, 'ROLE_ITEMS_CATEGORY_DELETE', 'DELETE', 24, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (25, 'Items Sub Category', 'Items Sub Category', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 23);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (241, 'ROLE_ITEMS_SUB_CATEGORY_VIEW', 'VIEW', 25, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(242, 'ROLE_ITEMS_SUB_CATEGORY_CREATE', 'CREATE', 25, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(243, 'ROLE_ITEMS_SUB_CATEGORY_EDIT', 'EDIT', 25, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(244, 'ROLE_ITEMS_SUB_CATEGORY_DELETE', 'DELETE', 25, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (26, 'Items', 'Items', 2, 'jeremi16us', '2016-11-18 00:00:00', 1, 24);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (251, 'ROLE_ITEMS_VIEW', 'VIEW', 26, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(252, 'ROLE_ITEMS_CREATE', 'CREATE', 26, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(253, 'ROLE_ITEMS_EDIT', 'EDIT', 26, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(254, 'ROLE_ITEMS_DELETE', 'DELETE', 26, 'jeremi16us', '2016-11-18 00:00:00', 1, 0),
(255, 'ROLE_ITEMS_UPDATE_FIELD', 'UPDATE FIELD', 26, 'jeremi16us', '2016-11-18 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (27, 'Price List', 'Price List', 2, 'jeremi16us', '2016-11-25 00:00:00', 1, 24);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (261, 'ROLE_ITEMS_PRICE_LIST_VIEW', 'VIEW', 27, 'jeremi16us', '2016-11-25 00:00:00', 1, 0),
(262, 'ROLE_ITEMS_PRICE_LIST_EDIT', 'UPDATE', 27, 'jeremi16us', '2016-11-25 00:00:00', 1, 0),
(263, 'ROLE_ITEMS_PRICE_LIST_UPDATE_PRICE_LIST_FROM_ANOTHER', 'UPDATE PRICE LIST FROM ANOTHER', 27, 'jeremi16us', '2016-11-25 00:00:00', 1, 0),
(264, 'ROLE_ITEMS_PRICE_LIST_NEW_PRICE_LIST_BY_COPY', 'NEW PRICE LIST BY COPY', 27, 'jeremi16us', '2016-11-25 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (28, 'Sponsor Non Supported Item', 'Sponsor Non Supported Item', 2, 'jeremi16us', '2016-11-25 00:00:00', 1, 25);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (271, 'ROLE_ITEMS_SPONSOR_NON_SUPPORTED_ITEMS_VIEW', 'VIEW', 28, 'jeremi16us', '2016-11-25 00:00:00', 1, 0),
(272, 'ROLE_ITEMS_SPONSOR_NON_SUPPORTED_ITEMS_ADD_REMOVE', 'ADD OR REMOVE', 28, 'jeremi16us', '2016-11-25 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (29, 'Items Reorder Level', 'Items Reorder Level', 2, 'jeremi16us', '2016-11-25 00:00:00', 1, 24);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (281, 'ROLE_ITEMS_REORDER_LEVEL_VIEW', 'VIEW', 29, 'jeremi16us', '2016-11-25 00:00:00', 1, 0),
(282, 'ROLE_ITEMS_REORDER_LEVEL_EDIT', 'UPDATE', 29, 'jeremi16us', '2016-11-25 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (30, 'Sponsor Allow Zero Price', 'Sponsor Allow Zero Price', 2, 'jeremi16us', '2016-11-28 00:00:00', 1, 26);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (291, 'ROLE_ITEMS_ALLOW_ZERO_PRICE_VIEW', 'VIEW', 30, 'jeremi16us', '2016-11-28 00:00:00', 1, 0),
(292, 'ROLE_ITEMS_ALLOW_ZERO_PRICE_ADD_REMOVE', 'ADD OR REMOVE', 30, 'jeremi16us', '2016-11-28 00:00:00', 1, 0);

##Reception Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (31, 'Reception Works', 'Reception Works', 3, 'jeremi16us', '2016-12-06 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (301, 'ROLE_RECEPTION_WORKS_VIEW', 'VIEW', 31, 'jeremi16us', '2016-12-06 00:00:00', 1, 1);

##Reception Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (32, 'Reports', 'Reports', 3, 'jeremi16us', '2016-12-06 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (311, 'ROLE_RECEPTION_PATIENT_VISIT_REPORT', 'PATIENT VISIT', 32, 'jeremi16us', '2016-12-06 00:00:00', 1, 1);

##Radiology Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (33, 'Radiology Works', 'Radiology Works', 4, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (321, 'ROLE_RADIOLOGY_WORKS_VIEW', 'VIEW', 33, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

##Radiology Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (34, 'Reports', 'Reports', 4, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (331, 'ROLE_RADIOLOGY_REPORT_VIEW', 'VIEW', 34, 'jeremi16us', '2016-12-07 00:00:00', 1, 1),
(332, 'ROLE_RADIOLOGY_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 34, 'jeremi16us', '2016-12-07 00:00:00', 1, 2),
(333, 'ROLE_RADIOLOGY_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 34, 'jeremi16us', '2016-12-07 00:00:00', 1, 3),
(334, 'ROLE_RADIOLOGY_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 34, 'jeremi16us', '2016-12-07 00:00:00', 1, 4),
(335, 'ROLE_RADIOLOGY_PERFORMANCE_REPORT', 'PERFORMANCE', 34, 'jeremi16us', '2016-12-08 00:00:00', 1, 5);

##Laboratory Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (35, 'Laboratory Works', 'Laboratory Works', 5, 'jeremi16us', '2016-12-13 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (341, 'ROLE_LABORATORY_WORKS_VIEW', 'VIEW', 35, 'jeremi16us', '2016-12-13 00:00:00', 1, 1),
       (342, 'ROLE_LABORATORY_WORKS_SUSPEND', 'ALLOW LABORATORY TO SUSPEND PATIENT', 35, 'Festus', '2023-01-19 00:00:00', 1, 1);

##Laboratory Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (36, 'Reports', 'Reports', 5, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (351, 'ROLE_LABORATORY_REPORT_VIEW', 'VIEW', 36, 'jeremi16us', '2016-12-13 00:00:00', 1, 1),
(352, 'ROLE_LABORATORY_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 36, 'jeremi16us', '2016-12-13 00:00:00', 1, 2),
(353, 'ROLE_LABORATORY_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 36, 'jeremi16us', '2016-12-13 00:00:00', 1, 3),
(354, 'ROLE_LABORATORY_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 36, 'jeremi16us', '2016-12-13 00:00:00', 1, 4),
(355, 'ROLE_LABORATORY_PERFORMANCE_REPORT', 'PERFORMANCE', 36, 'jeremi16us', '2016-12-13 00:00:00', 1, 5),
(356, 'ROLE_LABORATORY_RESULTS_REPORT', 'RESULTS REPORT', 36, 'jeremi16us', '2019-11-05 00:00:00', 1, 6);

##Store Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (37, 'Store Works', 'Store Works', 6, 'jeremi16us', '2016-12-14 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (361, 'ROLE_STORAGE_N_SUPPLY_WORKS_VIEW', 'VIEW', 37, 'jeremi16us', '2016-12-14 00:00:00', 1, 1);

##Store Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (38, 'Reports', 'Reports', 6, 'jeremi16us', '2016-12-07 00:00:00', 1, 11);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (371, 'ROLE_STORAGE_N_SUPPLY_STOCK_DETAILS_SUMMARY_REPORT', 'STOCK DETAILS SUMMARY', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 1),
(372, 'ROLE_STORAGE_N_SUPPLY_STOCK_LEDGER_REPORT', 'STOCK LEDGER', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 2),
(373, 'ROLE_STORAGE_N_SUPPLY_COMPARISON_STOCK_BALANCE_REPORT', 'COMPARISON STOCK BALANCE', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 3),
(374, 'ROLE_STORAGE_N_SUPPLY_ACCOUNT_PAYABLE_REPORT', 'ACCOUNT PAYABLE', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 4),
(375, 'ROLE_STORAGE_N_SUPPLY_PURCHASE_REPORT', 'PURCHASE REPORT', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 5),
(376, 'ROLE_STORAGE_N_SUPPLY_PURCHASE_HISTORY_REPORT', 'PURCHASE HISTORY', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 6),
(377, 'ROLE_STORAGE_N_SUPPLY_LAST_PRICE_REPORT', 'LAST PRICE', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 7),
(378, 'ROLE_STORAGE_N_SUPPLY_RETURN_INWARD_REPORT', 'RETURN INWARD', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 8),
(379, 'ROLE_STORAGE_N_SUPPLY_RETURN_OUTWARD_REPORT', 'RETURN OUTWARD', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 9),
(386, 'ROLE_STORAGE_N_SUPPLY_STOCK_DISPOSAL_REPORT', 'STOCK DISPOSAL', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 10),
(387, 'ROLE_STORAGE_N_SUPPLY_ISSUE_NOTE_REPORT', 'ISSUE NOTE', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 11),
(388, 'ROLE_STORAGE_N_SUPPLY_COST_OF_SALE_REPORT', 'COST OF SALE', 38, 'jeremi16us', '2016-12-14 00:00:00', 1, 12),
(389, 'ROLE_STORAGE_N_SUPPLY_STOCK_VARIANCE_REPORT', 'STOCK VARIANCE', 38, 'jeremi16us', '2017-03-22 00:00:00', 1, 13),
(396, 'ROLE_STORAGE_N_SUPPLY_STOCK_REPORT', 'STOCK REPORT', 38, 'jeremi16us', '2017-03-22 00:00:00', 1, 14),
(397, 'ROLE_STORAGE_N_SUPPLY_STOCK_MOVEMENT_REPORT', 'STOCK MOVEMENT REPORT', 38, 'jeremi16us', '2017-03-22 00:00:00', 1, 15),
(398, 'ROLE_STORAGE_N_SUPPLY_ITEM_BALANCE_REPORT', 'ITEM BALANCE REPORT', 38, 'joseph Incredible', '2017-03-22 00:00:00', 1, 16);

##Store Works Manage Stores With Stock Balance
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (39, 'Manage Stores With Stock Balance', 'Manage Stores With Stock Balance', 6, 'jeremi16us', '2016-12-14 00:00:00', 1, 10);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (381, 'ROLE_STORAGE_N_SUPPLY_MANAGE_STORES_WITH_STOCK_BALANCE', 'YES', 39, 'jeremi16us', '2016-12-14 00:00:00', 1, 1);

##Store Works Re-Order Level Notification
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (40, 'Re-Order Level Notification', 'Re-Order Level Notification', 6, 'jeremi16us', '2016-12-14 00:00:00', 1, 9);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (391, 'ROLE_STORAGE_N_SUPPLY_REORDER_LEVEL_NOTIFICATION', 'VIEW', 40, 'jeremi16us', '2016-12-14 00:00:00', 1, 1);

##Store Works Disposal
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (41, 'Disposal', 'Disposal', 6, 'jeremi16us', '2016-12-15 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (401, 'ROLE_STORAGE_N_SUPPLY_DISPOSAL_VIEW', 'VIEW', 41, 'jeremi16us', '2016-12-15 00:00:00', 1, 0),
(402, 'ROLE_STORAGE_N_SUPPLY_DISPOSAL_CREATE', 'CREATE', 41, 'jeremi16us', '2016-12-15 00:00:00', 1, 0),
(403, 'ROLE_STORAGE_N_SUPPLY_DISPOSAL_EDIT', 'EDIT', 41, 'jeremi16us', '2016-12-15 00:00:00', 1, 0),
(404, 'ROLE_STORAGE_N_SUPPLY_DISPOSAL_CANCEL', 'CANCEL', 41, 'jeremi16us', '2016-12-15 00:00:00', 1, 0);

##Store Works Return Inward
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (42, 'Return Inward', 'Return Inward', 6, 'jeremi16us', '2016-12-25 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (411, 'ROLE_STORAGE_N_SUPPLY_RETURN_INWARD_VIEW', 'VIEW', 42, 'jeremi16us', '2016-12-25 00:00:00', 1, 0),
(412, 'ROLE_STORAGE_N_SUPPLY_RETURN_INWARD_CREATE', 'CREATE', 42, 'jeremi16us', '2016-12-25 00:00:00', 1, 0),
(413, 'ROLE_STORAGE_N_SUPPLY_RETURN_INWARD_EDIT', 'EDIT', 42, 'jeremi16us', '2016-12-25 00:00:00', 1, 0),
(414, 'ROLE_STORAGE_N_SUPPLY_RETURN_INWARD_CANCEL', 'CANCEL', 42, 'jeremi16us', '2016-12-25 00:00:00', 1, 0);

##Store Works Return Outward
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (43, 'Return Outward', 'Return Outward', 6, 'jeremi16us', '2016-12-31 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (421, 'ROLE_STORAGE_N_SUPPLY_RETURN_OUTWARD_VIEW', 'VIEW', 43, 'jeremi16us', '2016-12-31 00:00:00', 1, 0),
(422, 'ROLE_STORAGE_N_SUPPLY_RETURN_OUTWARD_CREATE', 'CREATE', 43, 'jeremi16us', '2016-12-31 00:00:00', 1, 0),
(423, 'ROLE_STORAGE_N_SUPPLY_RETURN_OUTWARD_EDIT', 'EDIT', 43, 'jeremi16us', '2016-12-31 00:00:00', 1, 0),
(424, 'ROLE_STORAGE_N_SUPPLY_RETURN_OUTWARD_CANCEL', 'CANCEL', 43, 'jeremi16us', '2016-12-31 00:00:00', 1, 0);

##Store Works Requisition
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (44, 'Requisition', 'Requisition', 6, 'jeremi16us', '2017-01-02 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (431, 'ROLE_STORAGE_N_SUPPLY_REQUISITION_VIEW', 'VIEW', 44, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(432, 'ROLE_STORAGE_N_SUPPLY_REQUISITION_CREATE', 'CREATE', 44, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(433, 'ROLE_STORAGE_N_SUPPLY_REQUISITION_EDIT', 'EDIT', 44, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(434, 'ROLE_STORAGE_N_SUPPLY_REQUISITION_CANCEL', 'CANCEL', 44, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(435, 'ROLE_STORAGE_N_SUPPLY_REQUISITION_APPROVE', 'APPROVE', 44, 'jeremi16us', '2025-01-08 00:00:00', 1, 0);

##Store Works Issue Note Manual
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (45, 'Issue Note Manual', 'Issue Note Manual', 6, 'jeremi16us', '2017-01-02 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (441, 'ROLE_STORAGE_N_SUPPLY_ISSUE_MANUAL_VIEW', 'VIEW', 45, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(442, 'ROLE_STORAGE_N_SUPPLY_ISSUE_MANUAL_CREATE', 'CREATE', 45, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(443, 'ROLE_STORAGE_N_SUPPLY_ISSUE_MANUAL_EDIT', 'EDIT', 45, 'jeremi16us', '2017-01-02 00:00:00', 1, 0),
(444, 'ROLE_STORAGE_N_SUPPLY_ISSUE_MANUAL_CANCEL', 'CANCEL', 45, 'jeremi16us', '2017-01-02 00:00:00', 1, 0);

##Store Works Issue Note
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (46, 'Issue Note', 'Issue Note', 6, 'jeremi16us', '2017-01-04 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (451, 'ROLE_STORAGE_N_SUPPLY_ISSUE_VIEW', 'VIEW', 46, 'jeremi16us', '2017-01-04 00:00:00', 1, 0),
(452, 'ROLE_STORAGE_N_SUPPLY_ISSUE_CREATE', 'CREATE', 46, 'jeremi16us', '2017-01-04 00:00:00', 1, 0),
(453, 'ROLE_STORAGE_N_SUPPLY_ISSUE_EDIT', 'EDIT', 46, 'jeremi16us', '2017-01-04 00:00:00', 1, 0),
(454, 'ROLE_STORAGE_N_SUPPLY_ISSUE_CANCEL', 'CANCEL', 46, 'jeremi16us', '2017-01-04 00:00:00', 1, 0);

##Store Works Store Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (47, 'Store Order', 'Store Order', 6, 'jeremi16us', '2017-01-06 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (461, 'ROLE_STORAGE_N_SUPPLY_STORE_ORDER_VIEW', 'VIEW', 47, 'jeremi16us', '2017-01-06 00:00:00', 1, 0),
(462, 'ROLE_STORAGE_N_SUPPLY_STORE_ORDER_CREATE', 'CREATE', 47, 'jeremi16us', '2017-01-06 00:00:00', 1, 0),
(463, 'ROLE_STORAGE_N_SUPPLY_STORE_ORDER_EDIT', 'EDIT', 47, 'jeremi16us', '2017-01-06 00:00:00', 1, 0),
(464, 'ROLE_STORAGE_N_SUPPLY_STORE_ORDER_CANCEL', 'CANCEL', 47, 'jeremi16us', '2017-01-06 00:00:00', 1, 0),
(465, 'ROLE_STORAGE_N_SUPPLY_STORE_ORDER_APPROVE', 'APPROVE', 47, 'jeremi16us', '2017-01-06 00:00:00', 1, 0);

##Store Works GRN Against Issue Note
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (48, 'GRN Against Issue Note', 'GRN Against Issue Note', 6, 'jeremi16us', '2017-01-06 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (481, 'ROLE_STORAGE_N_SUPPLY_GRN_ISSUE_NOTE_VIEW', 'VIEW', 48, 'jeremi16us', '2017-01-06 00:00:00', 1, 0),
(482, 'ROLE_STORAGE_N_SUPPLY_GRN_ISSUE_NOTE_RECEIVE', 'RECEIVE', 48, 'jeremi16us', '2017-01-06 00:00:00', 1, 0),
(483, 'ROLE_STORAGE_N_SUPPLY_GRN_ISSUE_NOTE_CANCEL', 'CANCEL', 48, 'jeremi16us', '2017-01-06 00:00:00', 1, 0);

##Store Works Stock Taking
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (49, 'Stock Taking', 'Stock Taking', 6, 'jeremi16us', '2017-01-09 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (491, 'ROLE_STORAGE_N_SUPPLY_STOCK_TAKING_VIEW', 'VIEW', 49, 'jeremi16us', '2017-01-09 00:00:00', 1, 0),
(492, 'ROLE_STORAGE_N_SUPPLY_STOCK_TAKING_CREATE', 'CREATE', 49, 'jeremi16us', '2017-01-09 00:00:00', 1, 0),
(493, 'ROLE_STORAGE_N_SUPPLY_STOCK_TAKING_EDIT', 'EDIT', 49, 'jeremi16us', '2017-01-09 00:00:00', 1, 0),
(494, 'ROLE_STORAGE_N_SUPPLY_STOCK_TAKING_CANCEL', 'CANCEL', 49, 'jeremi16us', '2017-01-09 00:00:00', 1, 0),
(495, 'ROLE_STORAGE_N_SUPPLY_STOCK_TAKING_APPROVE', 'APPROVE', 49, 'jeremi16us', '2017-01-09 00:00:00', 1, 0);

##Store Works Grn Without Purchase Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (50, 'Grn Without Purchase Order', 'Grn Without Purchase Order', 6, 'jeremi16us', '2017-01-10 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (501, 'ROLE_STORAGE_N_SUPPLY_GRN_WITHOUT_PO_VIEW', 'VIEW', 50, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(502, 'ROLE_STORAGE_N_SUPPLY_GRN_WITHOUT_PO_CREATE', 'CREATE', 50, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(503, 'ROLE_STORAGE_N_SUPPLY_GRN_WITHOUT_PO_EDIT', 'EDIT', 50, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(504, 'ROLE_STORAGE_N_SUPPLY_GRN_WITHOUT_PO_CANCEL', 'CANCEL', 50, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(505, 'ROLE_STORAGE_N_SUPPLY_GRN_WITHOUT_PO_APPROVE', 'APPROVE', 50, 'jeremi16us', '2017-01-10 00:00:00', 1, 0);

##Store Works Grn With Purchase Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (51, 'Grn With Purchase Order', 'Grn With Purchase Order', 6, 'jeremi16us', '2017-01-10 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (511, 'ROLE_STORAGE_N_SUPPLY_GRN_WITH_PO_VIEW', 'VIEW', 51, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(512, 'ROLE_STORAGE_N_SUPPLY_GRN_WITH_PO_CREATE', 'CREATE', 51, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(513, 'ROLE_STORAGE_N_SUPPLY_GRN_WITH_PO_EDIT', 'EDIT', 51, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(514, 'ROLE_STORAGE_N_SUPPLY_GRN_WITH_PO_CANCEL', 'CANCEL', 51, 'jeremi16us', '2017-01-10 00:00:00', 1, 0),
(515, 'ROLE_STORAGE_N_SUPPLY_GRN_WITH_PO_APPROVE', 'APPROVE', 51, 'jeremi16us', '2017-01-10 00:00:00', 1, 0);

##Procurement Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (52, 'Procurement Works', 'Procurement Works', 7, 'jeremi16us', '2016-12-14 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (521, 'ROLE_PROCUREMENT_WORKS_VIEW', 'VIEW', 52, 'jeremi16us', '2016-12-14 00:00:00', 1, 1);

##Procurement Works Purchase Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (54, 'Purchase Order', 'Purchase Order', 7, 'jeremi16us', '2017-01-20 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (541, 'ROLE_PROCUREMENT_PURCHASE_ORDER_VIEW', 'VIEW', 54, 'jeremi16us', '2017-01-20 00:00:00', 1, 0),
(542, 'ROLE_PROCUREMENT_PURCHASE_ORDER_CREATE', 'CREATE', 54, 'jeremi16us', '2017-01-20 00:00:00', 1, 0),
(543, 'ROLE_PROCUREMENT_PURCHASE_ORDER_EDIT', 'EDIT', 54, 'jeremi16us', '2017-01-20 00:00:00', 1, 0),
(544, 'ROLE_PROCUREMENT_PURCHASE_ORDER_CANCEL', 'CANCEL', 54, 'jeremi16us', '2017-01-20 00:00:00', 1, 0),
(545, 'ROLE_PROCUREMENT_PURCHASE_ORDER_UPDATE_STATUS', 'UPDATE STATUS', 54, 'jeremi16us', '2020-03-18 00:00:00', 1, 0);

##Pharmacy Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (55, 'Pharmacy Works', 'Pharmacy Works', 8, 'jeremi16us', '2017-02-21 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (551, 'ROLE_PHARMACY_WORKS_VIEW', 'VIEW', 55, 'jeremi16us', '2017-02-21 00:00:00', 1, 1);

##Pharmacy Works Purchase Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (56, 'Dispense', 'Dispense', 8, 'jeremi16us', '2017-02-21 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (561, 'ROLE_PHARMACY_DISPENSE_VIEW', 'VIEW', 56, 'jeremi16us', '2017-02-21 00:00:00', 1, 0),
(562, 'ROLE_PHARMACY_DISPENSE_CREATE', 'CREATE', 56, 'jeremi16us', '2017-02-21 00:00:00', 1, 0),
(563, 'ROLE_PHARMACY_DISPENSE_EDIT', 'EDIT', 56, 'jeremi16us', '2017-02-21 00:00:00', 1, 0),
(564, 'ROLE_PHARMACY_DISPENSE_CANCEL', 'CANCEL', 56, 'jeremi16us', '2017-02-21 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (57, 'Patient', 'Patient', 3, 'jeremi16us', '2017-03-16 00:00:00', 1, 3);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (571, 'ROLE_RECEPTION_PATIENT_VIEW', 'VIEW', 57, 'jeremi16us', '2017-03-16 00:00:00', 1, 0),
(572, 'ROLE_RECEPTION_PATIENT_CREATE', 'CREATE', 57, 'jeremi16us', '2017-03-16 00:00:00', 1, 0),
(573, 'ROLE_RECEPTION_PATIENT_EDIT', 'EDIT', 57, 'jeremi16us', '2017-03-16 00:00:00', 1, 0),
(574, 'ROLE_RECEPTION_PATIENT_DELETE', 'DELETE', 57, 'jeremi16us', '2017-03-16 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (58, 'Patient Check In', 'Patient Check In', 9, 'jeremi16us', '2017-03-16 00:00:00', 1, 3);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (581, 'ROLE_REVENUE_CYCLE_PATIENT_CHECK_IN', 'YES', 58, 'jeremi16us', '2017-03-16 00:00:00', 1, 0);

# System Settings
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (59, 'System Settings', 'System Settings', 2, 'jeremi16us', '2017-03-16 00:00:00', 1, 30);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (591, 'ROLE_SYSTEM_SETTING_VIEW', 'VIEW', 59, 'jeremi16us', '2017-03-16 00:00:00', 1, 0),
       (593, 'ROLE_SYSTEM_SETTING_EDIT', 'EDIT', 59, 'jeremi16us', '2017-03-16 00:00:00', 1, 0);

#Confuguration
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (60, 'Configuration Settings', 'Configuration Settings', 2, 'jeremi16us', '2017-05-19 00:00:00', 1, 30);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (601, 'ROLE_CONFIGURATION_VIEW', 'VIEW', 60, 'jeremi16us', '2017-05-19 00:00:00', 1, 0),
       (602, 'ROLE_CONFIGURATION_EDIT', 'EDIT', 60, 'jeremi16us', '2017-05-19 00:00:00', 1, 0),
       (603, 'ROLE_CONFIGURATION_CREATE', 'CREATE', 60, 'jeremi16us', '2017-05-19 00:00:00', 1, 0),
       (604, 'ROLE_CONFIGURATION_DELETE', 'DELETE', 60, 'jeremi16us', '2017-05-19 00:00:00', 1, 0);

#Configuration option
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (61, 'Configuration Option Settings', 'Configuration Option Settings', 2, 'jeremi16us', '2017-05-19 00:00:00', 1, 30);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (611, 'ROLE_CONFIGURATION_OPTION_VIEW', 'VIEW', 61, 'jeremi16us', '2017-05-19 00:00:00', 1, 0),
       (612, 'ROLE_CONFIGURATION_OPTION_EDIT', 'EDIT', 61, 'jeremi16us', '2017-05-19 00:00:00', 1, 0),
       (613, 'ROLE_CONFIGURATION_OPTION_CREATE', 'CREATE', 61, 'jeremi16us', '2017-05-19 00:00:00', 1, 0),
       (614, 'ROLE_CONFIGURATION_OPTION_DELETE', 'DELETE', 61, 'jeremi16us', '2017-05-19 00:00:00', 1, 0);

# Disease Main Category
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (62, 'Disease Main Category', 'Disease Main Category', 2, 'jeremi16us', '2017-05-23 00:00:00', 1, 31);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (621, 'ROLE_DISEASE_MAIN_CATEGORY_VIEW', 'VIEW', 62, 'jeremi16us', '2017-05-23 00:00:00', 1, 0),
       (622, 'ROLE_DISEASE_MAIN_CATEGORY_CREATE', 'CREATE', 62, 'jeremi16us', '2017-05-23 00:00:00', 1, 0),
       (623, 'ROLE_DISEASE_MAIN_CATEGORY_EDIT', 'EDIT', 62, 'jeremi16us', '2017-05-23 00:00:00', 1, 0),
       (624, 'ROLE_DISEASE_MAIN_CATEGORY_DELETE', 'DELETE', 62, 'jeremi16us', '2017-05-23 00:00:00', 1, 0);

# Disease Category
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (63, 'Disease Category', 'Disease Category', 2, 'jeremi16us', '2017-05-23 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (631, 'ROLE_DISEASE_CATEGORY_VIEW', 'VIEW', 63, 'jeremi16us', '2017-05-23 00:00:00', 1, 0),
       (632, 'ROLE_DISEASE_CATEGORY_CREATE', 'CREATE', 63, 'jeremi16us', '2017-05-23 00:00:00', 1, 0),
       (633, 'ROLE_DISEASE_CATEGORY_EDIT', 'EDIT', 63, 'jeremi16us', '2017-05-23 00:00:00', 1, 0),
       (634, 'ROLE_DISEASE_CATEGORY_DELETE', 'DELETE', 63, 'jeremi16us', '2017-05-23 00:00:00', 1, 0);

# Disease SubCategory
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (64, 'Disease SubCategory', 'Disease SubCategory', 2, 'jeremi16us', '2017-05-26 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (641, 'ROLE_DISEASE_SUBCATEGORY_VIEW', 'VIEW', 64, 'jeremi16us', '2017-05-26 00:00:00', 1, 0),
       (642, 'ROLE_DISEASE_SUBCATEGORY_CREATE', 'CREATE', 64, 'jeremi16us', '2017-05-26 00:00:00', 1, 0),
       (643, 'ROLE_DISEASE_SUBCATEGORY_EDIT', 'EDIT', 64, 'jeremi16us', '2017-05-26 00:00:00', 1, 0),
       (644, 'ROLE_DISEASE_SUBCATEGORY_DELETE', 'DELETE', 64, 'jeremi16us', '2017-05-26 00:00:00', 1, 0);

# Disease Group
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (65, 'Disease Group', 'Disease Group', 2, 'jeremi16us', '2017-05-29 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (651, 'ROLE_DISEASE_GROUP_VIEW', 'VIEW', 65, 'jeremi16us', '2017-05-29 00:00:00', 1, 0),
       (652, 'ROLE_DISEASE_GROUP_CREATE', 'CREATE', 65, 'jeremi16us', '2017-05-29 00:00:00', 1, 0),
       (653, 'ROLE_DISEASE_GROUP_EDIT', 'EDIT', 65, 'jeremi16us', '2017-05-29 00:00:00', 1, 0),
       (654, 'ROLE_DISEASE_GROUP_DELETE', 'DELETE', 65, 'jeremi16us', '2017-05-29 00:00:00', 1, 0);

# Disease
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (66, 'Disease', 'Disease ', 2, 'jeremi16us', '2017-06-01 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (661, 'ROLE_DISEASE_VIEW', 'VIEW', 66, 'jeremi16us', '2017-06-01 00:00:00', 1, 0),
       (662, 'ROLE_DISEASE_CREATE', 'CREATE', 66, 'jeremi16us', '2017-06-01 00:00:00', 1, 0),
       (663, 'ROLE_DISEASE_EDIT', 'EDIT', 66, 'jeremi16us', '2017-06-01 00:00:00', 1, 0),
       (664, 'ROLE_DISEASE_DELETE', 'DELETE', 66, 'jeremi16us', '2017-06-01 00:00:00', 1, 0),
       (665, 'ROLE_DISEASE_CODE_VIEW', 'VIEW CODE', 66, 'jeremi16us', '2017-06-01 00:00:00', 1, 0);

# Laboratory Parameter
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (67, 'LABORATORY PARAMETER', 'LABORATORY PARAMETER ', 5, 'jeremi16us', '2017-06-09 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (671, 'ROLE_LABORATORY_PARAMETER_VIEW', 'VIEW', 67, 'jeremi16us', '2017-06-09 00:00:00', 1, 0),
       (672, 'ROLE_LABORATORY_PARAMETER_CREATE', 'CREATE', 67, 'jeremi16us', '2017-06-09 00:00:00', 1, 0),
       (673, 'ROLE_LABORATORY_PARAMETER_EDIT', 'EDIT', 67, 'jeremi16us', '2017-06-09 00:00:00', 1, 0),
       (674, 'ROLE_LABORATORY_PARAMETER_DELETE', 'DELETE', 67, 'jeremi16us', '2017-06-09 00:00:00', 1, 0);

# Laboratory Specimen
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (68, 'LABORATORY SPECIMEN', 'LABORATORY SPECIMEN ', 5, 'jeremi16us', '2017-06-11 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (681, 'ROLE_LABORATORY_SPECIMEN_VIEW', 'VIEW', 68, 'jeremi16us', '2017-06-11 00:00:00', 1, 0),
       (682, 'ROLE_LABORATORY_SPECIMEN_CREATE', 'CREATE', 68, 'jeremi16us', '2017-06-11 00:00:00', 1, 0),
       (683, 'ROLE_LABORATORY_SPECIMEN_EDIT', 'EDIT', 68, 'jeremi16us', '2017-06-11 00:00:00', 1, 0),
       (684, 'ROLE_LABORATORY_SPECIMEN_DELETE', 'DELETE', 68, 'jeremi16us', '2017-06-11 00:00:00', 1, 0);

# Laboratory Setup
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (69, 'LABORATORY TEST SETUP', 'LABORATORY TEST SETUP ', 5, 'jeremi16us', '2017-06-11 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (691, 'ROLE_LABORATORY_TEST_SETUP_VIEW', 'VIEW', 69, 'jeremi16us', '2017-06-11 00:00:00', 1, 0),
       (692, 'ROLE_LABORATORY_TEST_SETUP_MANAGE_SPECIMEN', 'MANAGE SPECIMEN', 69, 'jeremi16us', '2017-06-11 00:00:00', 1, 0),
       (693, 'ROLE_LABORATORY_TEST_SETUP_MANAGE_PARAMETER', 'MANAGE PARAMETER', 69, 'jeremi16us', '2017-06-11 00:00:00', 1, 0),
       (694, 'ROLE_LABORATORY_TEST_SETUP_MANAGE_AVAILABILITY', 'MANAGE AVAILABILITY', 69, 'jeremi16us', '2017-06-11 00:00:00', 1, 0);

##Admission Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (70, 'Admission Works', 'Admission Works', 10, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (701, 'ROLE_ADMISSION_WORKS_VIEW', 'VIEW', 70, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

##Doctors Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (71, 'Doctor\'s Outpatient Works', 'Doctor\'s Outpatient Works', 11, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (711, 'ROLE_DOCTOR_OUTPATIENT_WORKS_VIEW', 'VIEW', 71, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (72, 'Doctor\'s Inpatient Works', 'Doctor\'s Inpatient Works', 11, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (721, 'ROLE_DOCTOR_INPATIENT_WORKS_VIEW', 'VIEW', 72, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

##Dialysis Works
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (73, 'Dialysis Works', 'Dialysis Works', 12, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (731, 'ROLE_DIALYSIS_WORKS_VIEW', 'VIEW', 73, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

##Revenue Cycle
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (74, 'Revenue Cycle', 'Revenue Cycle', 9, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (741, 'ROLE_REVENUE_CYCLE_WORKS_VIEW', 'VIEW', 74, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
       (742, 'ROLE_REVENUE_CYCLE_ADHOC_VIEW', 'ADHOC_VIEW', 74, 'josephKiwia', '2023-01-18 00:00:00', 1, 0),
       (743, 'ROLE_REVENUE_CYCLE_CASH_PAYMENTS', 'CASH PAYMENTS', 74, 'Festus', '2023-01-19 00:00:00', 1, 0),
       (744, 'ROLE_REVENUE_CYCLE_CREDIT_PAYMENTS', 'CREDIT PAYMENTS', 74, 'Festus', '2023-01-19 00:00:00', 1, 0); 

##General Ledger
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (75, 'General Ledger', 'General Ledger', 13, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (751, 'ROLE_GENERAL_LEDGER_WORKS_VIEW', 'VIEW', 75, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

##Surgery
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (76, 'Surgery Works', 'Surgery Works', 14, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (761, 'ROLE_SURGERY_WORKS_VIEW', 'VIEW', 76, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);



##Procedure
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (77, 'Procedure Works', 'Procedure Works', 15, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (771, 'ROLE_PROCEDURE_WORKS_VIEW', 'VIEW', 77, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

##Patient Record
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (78, 'Patient Record', 'Patient Record', 16, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (781, 'ROLE_PATIENT_RECORD_VIEW', 'VIEW', 78, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
       (782, 'ROLE_PATIENT_RECORD_MERGE_FILE', 'MERGE FILE', 78, 'jeremi16us', '2018-01-03 00:00:00', 1, 0),
       (783, 'ROLE_PATIENT_RECORD_CHANGE_CHECK_IN_STATUS', 'CHANGE CHECK IN STATUS', 78, 'jeremi16us', '2018-02-28 00:00:00', 1, 0),
       (772, 'ROLE_PATIENT_RECORD_CHANGE_CLAIM_PATIENT_SIGNATURE', 'COPY PATIENT SIGNATURE', 78, 'jeremi16us', '2018-02-28 00:00:00', 1, 0),
       (784, 'ROLE_PATIENT_RECORD_OVERALL_RESULTS', 'OVERALL RESULTS', 78, 'jeremi16us', '2018-02-28 00:00:00', 1, 0),
       (785, 'ROLE_PATIENT_RECORD_SUMMARY_PATIENT_FILE', 'SUMMARY PATIENT FILE', 78, 'jeremi16us', '2018-02-28 00:00:00', 1, 0),
       (786, 'ROLE_PATIENT_RECORD_MANAGE_PATIENT_FILE_STATUS', 'MANAGE PATIENT FILE STATUS', 78, 'jeremi16us', '2018-02-28 00:00:00', 1, 0),
       (787, 'ROLE_PATIENT_RECORD_ALLOW_TO_REVIEW_ANY_RECORD', 'ALLOW TO REVIEW ANY RECORD', 78, 'jeremi16us', '2018-02-28 00:00:00', 1, 0),
       (788, 'ROLE_PATIENT_RECORD_ALLOW_TO_PRINT_SUMMARY', 'ALLOW TO PRINT SUMMARY', 78, 'jeremi16us', '2019-10-16 00:00:00', 1, 0),
       (789, 'ROLE_PATIENT_RECORD_ALLOW_TO_SPLIT_CLAIM', 'ALLOW TO SPLIT CLAIM', 78, 'jeremi16us', '2019-10-30 00:00:00', 1, 0),
       (790, 'ROLE_PATIENT_RECORD_ALLOW_TO_MERGE_CLAIM', 'ALLOW TO MERGE CLAIM', 78, 'jeremi16us', '2020-06-17 00:00:00', 1, 0),
       (795, 'ROLE_PATIENT_RECORD_ALLOW_TO_DUPLICATE_CLAIM', 'ALLOW TO DUPLICATE CLAIM', 78, 'jeremi16us', '2020-06-17 00:00:00', 1, 0);

##Management
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (79, 'Management', 'Management', 17, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (791, 'ROLE_MANAGEMENT_WORKS_VIEW', 'VIEW', 79, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

##Quality Assurance
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (80, 'Quality Assurance Works', 'Quality Assurance Works', 18, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (801, 'ROLE_QUALITY_ASSURANCE_WORKS_VIEW', 'VIEW', 80, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

##Patient Billing
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (81, 'Patient Billing Works', 'Patient Billing Works', 19, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (811, 'ROLE_PATIENT_BILLING_WORKS_VIEW', 'VIEW', 81, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

##Optical
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (82, 'Optical', 'Optical', 20, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (821, 'ROLE_OPTICAL_WORKS_VIEW', 'VIEW', 82, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

#Broadcast Message
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (83, 'Broadcast Message', 'Broadcast Message', 2, 'jeremi16us', '2017-06-29 00:00:00', 1, 30);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (831, 'ROLE_BROADCAST_MESSAGE_VIEW', 'VIEW', 83, 'jeremi16us', '2017-06-29 00:00:00', 1, 0),
       (832, 'ROLE_BROADCAST_MESSAGE_EDIT', 'EDIT', 83, 'jeremi16us', '2017-06-29 00:00:00', 1, 0),
       (833, 'ROLE_BROADCAST_MESSAGE_CREATE', 'CREATE', 83, 'jeremi16us', '2017-06-29 00:00:00', 1, 0),
       (834, 'ROLE_BROADCAST_MESSAGE_DELETE', 'DELETE', 83, 'jeremi16us', '2017-06-29 00:00:00', 1, 0);

#Dialysis Notes
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (84, 'Dialysis Notes', 'Dialysis Notes', 12, 'jeremi16us', '2017-07-19 00:00:00', 1, 30);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (841, 'ROLE_DIALYSIS_NOTES_VIEW', 'VIEW', 84, 'jeremi16us', '2017-07-19 00:00:00', 1, 0),
       (842, 'ROLE_DIALYSIS_NOTES_EDIT', 'EDIT', 84, 'jeremi16us', '2017-07-19 00:00:00', 1, 0),
       (843, 'ROLE_DIALYSIS_NOTES_CREATE', 'CREATE', 84, 'jeremi16us', '2017-07-19 00:00:00', 1, 0),
       (844, 'ROLE_DIALYSIS_NOTES_DELETE', 'DELETE', 84, 'jeremi16us', '2017-07-19 00:00:00', 1, 0);

#Dialysis Patient Assessment
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (85, 'Dialysis Patient Assessment', 'Dialysis Patient Assessment', 12, 'jeremi16us', '2017-07-20 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (851, 'ROLE_DIALYSIS_PATIENT_ASSESSMENT_VIEW', 'VIEW', 85, 'jeremi16us', '2017-07-20 00:00:00', 1, 0),
       (852, 'ROLE_DIALYSIS_PATIENT_ASSESSMENT_EDIT', 'EDIT', 85, 'jeremi16us', '2017-07-20 00:00:00', 1, 0),
       (853, 'ROLE_DIALYSIS_PATIENT_ASSESSMENT_CREATE', 'CREATE', 85, 'jeremi16us', '2017-07-20 00:00:00', 1, 0),
       (854, 'ROLE_DIALYSIS_PATIENT_ASSESSMENT_DELETE', 'DELETE', 85, 'jeremi16us', '2017-07-20 00:00:00', 1, 0);

#Dialysis Machine Assessment
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (86, 'Dialysis Machine Assessment', 'Dialysis Machine Assessment', 12, 'jeremi16us', '2017-07-20 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (861, 'ROLE_DIALYSIS_MACHINE_ASSESSMENT_VIEW', 'VIEW', 86, 'jeremi16us', '2017-07-20 00:00:00', 1, 0),
       (862, 'ROLE_DIALYSIS_MACHINE_ASSESSMENT_EDIT', 'EDIT', 86, 'jeremi16us', '2017-07-20 00:00:00', 1, 0),
       (863, 'ROLE_DIALYSIS_MACHINE_ASSESSMENT_CREATE', 'CREATE', 86, 'jeremi16us', '2017-07-20 00:00:00', 1, 0),
       (864, 'ROLE_DIALYSIS_MACHINE_ASSESSMENT_DELETE', 'DELETE', 86, 'jeremi16us', '2017-07-20 00:00:00', 1, 0);

#Dialysis Orders Assessment
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (87, 'Dialysis Orders Assessment', 'Dialysis Orders Assessment', 12, 'jeremi16us', '2017-07-21 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (871, 'ROLE_DIALYSIS_DIALYSIS_ORDERS_VIEW', 'VIEW', 87, 'jeremi16us', '2017-07-21 00:00:00', 1, 0),
       (872, 'ROLE_DIALYSIS_DIALYSIS_ORDERS_EDIT', 'EDIT', 87, 'jeremi16us', '2017-07-21 00:00:00', 1, 0),
       (873, 'ROLE_DIALYSIS_DIALYSIS_ORDERS_CREATE', 'CREATE', 87, 'jeremi16us', '2017-07-21 00:00:00', 1, 0),
       (874, 'ROLE_DIALYSIS_DIALYSIS_ORDERS_DELETE', 'DELETE', 87, 'jeremi16us', '2017-07-21 00:00:00', 1, 0);

#Dialysis Heparain
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (88, 'Dialysis Heparain', 'Dialysis Heparain', 12, 'jeremi16us', '2017-07-21 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (881, 'ROLE_DIALYSIS_HEPARAIN_VIEW', 'VIEW', 88, 'jeremi16us', '2017-07-21 00:00:00', 1, 0),
       (882, 'ROLE_DIALYSIS_HEPARAIN_EDIT', 'EDIT', 88, 'jeremi16us', '2017-07-21 00:00:00', 1, 0),
       (883, 'ROLE_DIALYSIS_HEPARAIN_CREATE', 'CREATE', 88, 'jeremi16us', '2017-07-21 00:00:00', 1, 0),
       (884, 'ROLE_DIALYSIS_HEPARAIN_DELETE', 'DELETE', 88, 'jeremi16us', '2017-07-21 00:00:00', 1, 0);

#Surgery Post Operative Notes
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (89, 'Post Operative Notes', 'Post Operative Notes', 14, 'jeremi16us', '2017-07-26 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (891, 'ROLE_SURGERY_POST_OPERATIVE_NOTES_VIEW', 'VIEW', 89, 'jeremi16us', '2017-07-26 00:00:00', 1, 0),
       (892, 'ROLE_SURGERY_POST_OPERATIVE_NOTES_EDIT', 'EDIT', 89, 'jeremi16us', '2017-07-26 00:00:00', 1, 0),
       (893, 'ROLE_SURGERY_POST_OPERATIVE_NOTES_CREATE', 'CREATE', 89, 'jeremi16us', '2017-07-26 00:00:00', 1, 0),
       (894, 'ROLE_SURGERY_POST_OPERATIVE_NOTES_DELETE', 'DELETE', 89, 'jeremi16us', '2017-07-26 00:00:00', 1, 0);

#Doctor Consultation Clinical Notes
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (90, 'Clinical Notes', 'Clinical Notes', 11, 'jeremi16us', '2017-08-03 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (901, 'ROLE_DOCTOR_OUTPATIENT_CLINICAL_NOTES_VIEW', 'VIEW', 90, 'jeremi16us', '2017-08-03 00:00:00', 1, 0),
       (902, 'ROLE_DOCTOR_OUTPATIENT_CLINICAL_NOTES_EDIT', 'EDIT', 90, 'jeremi16us', '2017-08-03 00:00:00', 1, 0),
       (903, 'ROLE_DOCTOR_OUTPATIENT_CLINICAL_NOTES_CREATE', 'CREATE', 90, 'jeremi16us', '2017-08-03 00:00:00', 1, 0),
       (904, 'ROLE_DOCTOR_OUTPATIENT_CLINICAL_NOTES_DELETE', 'DELETE', 90, 'jeremi16us', '2017-08-03 00:00:00', 1, 0);

#Patient Admission
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (91, 'Patient Admission', 'Patient Admission', 10, 'jeremi16us', '2017-08-04 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (911, 'ROLE_PATIENT_ADMISSION_VIEW', 'VIEW', 91, 'jeremi16us', '2017-08-04 00:00:00', 1, 0),
       (912, 'ROLE_PATIENT_ADMISSION_EDIT', 'EDIT', 91, 'jeremi16us', '2017-08-04 00:00:00', 1, 0),
       (913, 'ROLE_PATIENT_ADMISSION_CREATE', 'CREATE', 91, 'jeremi16us', '2017-08-04 00:00:00', 1, 0),
       (914, 'ROLE_PATIENT_ADMISSION_DELETE', 'DELETE', 91, 'jeremi16us', '2017-08-04 00:00:00', 1, 0);

#Nurse Actions
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (100, 'Nurse Actions', 'Nurse Actions', 10, 'jeremi16us', '2017-08-05 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1001, 'ROLE_NURSE_ACTION_CANCEL_ADMISSION', 'CANCEL ADMISSION', 100, 'jeremi16us', '2017-08-05 00:00:00', 1, 0),
       (1002, 'ROLE_NURSE_ACTION_WARD_TRANSFER', 'WARD TRANSFER', 100, 'jeremi16us', '2017-08-05 00:00:00', 1, 0),
       (1003, 'ROLE_NURSE_ACTION_CANCEL_DISCHARGE', 'CANCEL DISCHARGE', 100, 'jeremi16us', '2017-08-05 00:00:00', 1, 0),
       (1004, 'ROLE_NURSE_ACTION_DELETE', 'DELETE', 100, 'jeremi16us', '2017-08-05 00:00:00', 1, 0);

#Patient Progress
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (101, 'Patient Progress', 'Patient Progress', 10, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1011, 'ROLE_PATIENT_PROGRESS_VIEW', 'VIEW', 101, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1012, 'ROLE_PATIENT_PROGRESS_EDIT', 'EDIT', 101, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1013, 'ROLE_PATIENT_PROGRESS_CREATE', 'CREATE', 101, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1014, 'ROLE_PATIENT_PROGRESS_DELETE', 'DELETE', 101, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

#Patient Blood Glucose
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (102, 'Patient Blood Glucose', 'Patient Blood Glucose', 10, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1021, 'ROLE_PATIENT_BLOOD_GLUCOSE_VIEW', 'VIEW', 102, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1022, 'ROLE_PATIENT_BLOOD_GLUCOSE_EDIT', 'EDIT', 102, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1023, 'ROLE_PATIENT_BLOOD_GLUCOSE_CREATE', 'CREATE', 102, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1024, 'ROLE_PATIENT_BLOOD_GLUCOSE_DELETE', 'DELETE', 102, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

#Patient Intake and Output
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (103, 'Patient Intake and Output', 'Patient Intake and Output', 10, 'jeremi16us', '2017-08-10 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1031, 'ROLE_PATIENT_INTAKE_OUTPUT_VIEW', 'VIEW', 103, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1032, 'ROLE_PATIENT_INTAKE_OUTPUT_EDIT', 'EDIT', 103, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1033, 'ROLE_PATIENT_INTAKE_OUTPUT_CREATE', 'CREATE', 103, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1034, 'ROLE_PATIENT_INTAKE_OUTPUT_DELETE', 'DELETE', 103, 'jeremi16us', '2017-08-10 00:00:00', 1, 0);

#Patient Observation
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (104, 'Patient Observation', 'Patient Observation', 10, 'jeremi16us', '2017-08-10 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1041, 'ROLE_PATIENT_OBSERVATION_VIEW', 'VIEW', 104, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1042, 'ROLE_PATIENT_OBSERVATION_EDIT', 'EDIT', 104, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1043, 'ROLE_PATIENT_OBSERVATION_CREATE', 'CREATE', 104, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1044, 'ROLE_PATIENT_OBSERVATION_DELETE', 'DELETE', 104, 'jeremi16us', '2017-08-10 00:00:00', 1, 0);

#Nurse Care Plan
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (105, 'Nurse Care Plan', 'Nurse Care Plan', 10, 'jeremi16us', '2017-08-10 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1051, 'ROLE_NURSE_CARE_PLAN_VIEW', 'VIEW', 105, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1052, 'ROLE_NURSE_CARE_PLAN_EDIT', 'EDIT', 105, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1053, 'ROLE_NURSE_CARE_PLAN_CREATE', 'CREATE', 105, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1054, 'ROLE_NURSE_CARE_PLAN_DELETE', 'DELETE', 105, 'jeremi16us', '2017-08-10 00:00:00', 1, 0);

#Patient Malnutrition Observation
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (106, 'Patient Malnutrition Observation', 'Patient Malnutrition Observation', 10, 'jeremi16us', '2017-08-10 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1061, 'ROLE_PATIENT_MALNUTRITION_OBSERVATION_VIEW', 'VIEW', 106, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1062, 'ROLE_PATIENT_MALNUTRITION_OBSERVATION_EDIT', 'EDIT', 106, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1063, 'ROLE_PATIENT_MALNUTRITION_OBSERVATION_CREATE', 'CREATE', 106, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1064, 'ROLE_PATIENT_MALNUTRITION_OBSERVATION_DELETE', 'DELETE', 106, 'jeremi16us', '2017-08-10 00:00:00', 1, 0);

#Pediatric Observation
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (107, 'Pediatric Observation', 'Pediatric Observation', 10, 'jeremi16us', '2017-08-10 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1071, 'ROLE_PEDIATRIC_OBSERVATION_VIEW', 'VIEW', 107, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1072, 'ROLE_PEDIATRIC_OBSERVATION_EDIT', 'EDIT', 107, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1073, 'ROLE_PEDIATRIC_OBSERVATION_CREATE', 'CREATE', 107, 'jeremi16us', '2017-08-10 00:00:00', 1, 0),
       (1074, 'ROLE_PEDIATRIC_OBSERVATION_DELETE', 'DELETE', 107, 'jeremi16us', '2017-08-10 00:00:00', 1, 0);

#Patient Pre-operative Checklist
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (108, 'Patient Pre-operative Checklist', 'Patient Pre-operative Checklist', 10, 'jeremi16us', '2017-08-14 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1081, 'ROLE_PATIENT_PREOPERATIVE_CHECKLIST_VIEW', 'VIEW', 108, 'jeremi16us', '2017-08-14 00:00:00', 1, 0),
       (1082, 'ROLE_PATIENT_PREOPERATIVE_CHECKLIST_EDIT', 'EDIT', 108, 'jeremi16us', '2017-08-14 00:00:00', 1, 0),
       (1083, 'ROLE_PATIENT_PREOPERATIVE_CHECKLIST_CREATE', 'CREATE', 108, 'jeremi16us', '2017-08-14 00:00:00', 1, 0),
       (1084, 'ROLE_PATIENT_PREOPERATIVE_CHECKLIST_DELETE', 'DELETE', 108, 'jeremi16us', '2017-08-14 00:00:00', 1, 0);

##Store Works Grn As Open Balance
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (109, 'Grn As Open Balance', 'Grn As Open Balance', 6, 'jeremi16us', '2017-08-23 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1091, 'ROLE_STORAGE_N_SUPPLY_GRN_AS_OPEN_BALANCE_VIEW', 'VIEW', 109, 'jeremi16us', '2017-08-23 00:00:00', 1, 0),
(1092, 'ROLE_STORAGE_N_SUPPLY_GRN_AS_OPEN_BALANCE_CREATE', 'CREATE', 109, 'jeremi16us', '2017-08-23 00:00:00', 1, 0),
(1093, 'ROLE_STORAGE_N_SUPPLY_GRN_AS_OPEN_BALANCE_EDIT', 'EDIT', 109, 'jeremi16us', '2017-08-23 00:00:00', 1, 0),
(1094, 'ROLE_STORAGE_N_SUPPLY_GRN_AS_OPEN_BALANCE_CANCEL', 'CANCEL', 109, 'jeremi16us', '2017-08-23 00:00:00', 1, 0),
(1095, 'ROLE_STORAGE_N_SUPPLY_GRN_AS_OPEN_BALANCE_APPROVE', 'APPROVE', 109, 'jeremi16us', '2017-08-23 00:00:00', 1, 0);


##Doctor Consultation Fee
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (110, 'Doctor Consultation Fee', 'Doctor Consultation Fee', 2, 'jeremi16us', '2017-09-12 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1101, 'ROLE_DOCTOR_CONSULTATION_FEE_VIEW', 'VIEW', 110, 'jeremi16us', '2017-09-12 00:00:00', 1, 0),
(1102, 'ROLE_DOCTOR_CONSULTATION_FEE_CREATE', 'CREATE', 110, 'jeremi16us', '2017-09-12 00:00:00', 1, 0),
(1103, 'ROLE_DOCTOR_CONSULTATION_FEE_EDIT', 'EDIT', 110, 'jeremi16us', '2017-09-12 00:00:00', 1, 0),
(1104, 'ROLE_DOCTOR_CONSULTATION_FEE_DELETE', 'DELETE', 110, 'jeremi16us', '2017-09-12 00:00:00', 1, 0),
(1105, 'ROLE_ASSIGN_EMPLOYEE_TO_DOCTOR_CONSULTATION_CATEGORY', 'ASSIGN EMPLOYEE TO DOCTOR CONSULTATION CATEGORY', 110, 'jeremi16us', '2017-09-12 00:00:00', 1, 0);

##Doctor Consultation Fee
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (120, 'DHIS 2 Report', 'DHIS 2 Report', 21, 'jeremi16us', '2017-09-12 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1201, 'ROLE_DHIS_REPORT_VIEW', 'VIEW', 120, 'jeremi16us', '2017-09-12 00:00:00', 1, 0);

##Billing Manupulation
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (121, 'Billing Manupulation', 'Billing Manupulation', 19, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1211, 'ROLE_BILLING_MANUPULATION_ZERO_ITEM_BILL', 'ZERO ITEM BILL', 121, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1212, 'ROLE_BILLING_MANUPULATION_REMOVE_CREDIT_BILL_ITEM', 'REMOVE CREDIT BILL ITEM', 121, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1213, 'ROLE_BILLING_MANUPULATION_REMOVE_CASH_BILL_ITEM', 'REMOVE CASH BILL ITEM', 121, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1214, 'ROLE_BILLING_MANIPULATION_BY_PASS_ADMISSION_CHARGES', 'BY PASS ADMISSION CHARGES', 121, 'jeremi16us', '2019-10-08 00:00:00', 1, 0),
(1215, 'ROLE_BILLING_MANIPULATION_BY_PASS_CASH_BILL', 'BY PASS CASH BILL', 121, 'jeremi16us', '2019-10-08 00:00:00', 1, 0);

##Edit Transaction
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (122, 'Edit Transaction', 'Edit Transaction', 13, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1221, 'ROLE_EDIT_TRANSACTION_CANCEL_CASH', 'CANCEL CASH', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1222, 'ROLE_EDIT_TRANSACTION_CANCEL_CREDIT', 'CANCEL CREDIT', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1223, 'ROLE_EDIT_TRANSACTION_REMOVE_CASH_ITEM', 'REMOVE CASH ITEM', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1224, 'ROLE_EDIT_TRANSACTION_REMOVE_CREDIT_ITEM', 'REMOVE CREDIT ITEM', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1225, 'ROLE_EDIT_TRANSACTION_EDIT_CASH_ITEM', 'EDIT CASH ITEM', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1226, 'ROLE_EDIT_TRANSACTION_EDIT_CREDIT_ITEM', 'EDIT CREDIT ITEM', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1227, 'ROLE_EDIT_TRANSACTION_CHANGE_BILL_TYPE_CASH', 'CHANGE BILLING TYPE CASH', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1228, 'ROLE_EDIT_TRANSACTION_CHANGE_BILL_TYPE_CREDIT', 'CHANGE BILLING TYPE CREDIT', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1229, 'ROLE_EDIT_TRANSACTION_ADD_ITEM_CASH', 'ADD ITEM CASH', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1230, 'ROLE_EDIT_TRANSACTION_ADD_ITEM_CREDIT', 'ADD ITEM CREDIT', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1233, 'ROLE_EDIT_TRANSACTION_SWAP_TO_ANY_ITEM', 'SWAP TO ANY ITEM', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1234, 'ROLE_EDIT_TRANSACTION_MIMIC_TIME_EMPLOYEE', 'MIMIC TIME AND EMPLOYEE', 122, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

##Discharge & UnClearBill
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (123, 'Discharge and Bill', 'Discharge and Bill', 19, 'jeremi16us', '2017-10-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1231, 'ROLE_DISCHARGE_PATIENT', 'DISCHARGE', 123, 'jeremi16us', '2017-10-07 00:00:00', 1, 0),
       (1232, 'ROLE_UNCLEAR_BILL', 'UNCLEAR BILL', 123, 'jeremi16us', '2017-10-07 00:00:00', 1, 0),
       (1235, 'ROLE_DISCHARGE_PATIENT_WITHOUT_CLEARING_BILL', 'DISCHARGE W/O CLEARING BILL', 123, 'jeremi16us', '2017-10-07 00:00:00', 1, 0),
        (1236, 'ROLE_INPATIENT_CLEAR_BILL', 'CLEAR BILL', 123, 'josephIncredible', '2024-09-17 00:00:00', 1, 1),
        (1237, 'ROLE_INPATIENT_APPROVE_BILL', 'APPROVE BILL', 123, 'josephIncredible', '2024-09-17 00:00:00', 1, 1);

##Pharmacy Works Purchase Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (124, 'Patient From Outside', 'Patient From Outside', 8, 'jeremi16us', '2017-10-12 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1241, 'ROLE_PHARMACY_FROM_OUTSIDE_VIEW', 'VIEW', 124, 'jeremi16us', '2017-10-12 00:00:00', 1, 0),
(1242, 'ROLE_PHARMACY_FROM_OUTSIDE_TRANSACT', 'TRANSACT', 124, 'jeremi16us', '2017-10-12 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (125, 'Patient', 'Patient', 3, 'jeremi16us', '2017-10-12 00:00:00', 1, 3);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1251, 'ROLE_RECEPTION_PATIENT_REGISTRATION_REGISTER_ALL_SPONSOR', 'REGISTER ALL SPONSOR', 125, 'jeremi16us', '2017-10-12 00:00:00', 1, 0),
(1252, 'ROLE_RECEPTION_PATIENT_REGISTRATION_EDIT_CASH_PATIENT', 'EDIT CASH PATIENT', 125, 'jeremi16us', '2017-10-12 00:00:00', 1, 0),
(1253, 'ROLE_RECEPTION_PATIENT_REGISTRATION_EDIT_NO_RESTRICTION_ON_PATIENT_NAME_SIMILARITY', 'NO RESTRICTION ON PATIENT NAME SIMILARITY', 125, 'jeremi16us', '2019-07-18 00:00:00', 1, 0);

##Surgery Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (126, 'Reports', 'Reports', 14, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES(1261, 'ROLE_SURGERY_REPORT_VIEW', 'VIEW', 126, 'jeremi16us', '2017-10-13 00:00:00', 1, 1),
(1262, 'ROLE_SURGERY_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 126, 'jeremi16us', '2017-10-13 00:00:00', 1, 2),
(1263, 'ROLE_SURGERY_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 126, 'jeremi16us', '2017-10-13 00:00:00', 1, 3),
(1264, 'ROLE_SURGERY_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 126, 'jeremi16us', '2017-10-13 00:00:00', 1, 4),
(1265, 'ROLE_SURGERY_PERFORMANCE_REPORT', 'PERFORMANCE', 126, 'jeremi16us', '2017-10-13 00:00:00', 1, 5),
(1266, 'ROLE_SURGERY_PERFORMANCE_REPORT_FOR_DOCTOR_ONLY', 'PERFORMANCE FOR DOCTOR ONLY', 126, 'jeremi16us', '2017-10-13 00:00:00', 1, 6);

##Procedure Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (127, 'Reports', 'Reports', 15, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES(1271, 'ROLE_PROCEDURE_REPORT_VIEW', 'VIEW', 127, 'jeremi16us', '2017-10-13 00:00:00', 1, 1),
 (1272, 'ROLE_PROCEDURE_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 127, 'jeremi16us', '2017-10-13 00:00:00', 1, 2),
(1273, 'ROLE_PROCEDURE_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 127, 'jeremi16us', '2017-10-13 00:00:00', 1, 3),
(1274, 'ROLE_PROCEDURE_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 127, 'jeremi16us', '2017-10-13 00:00:00', 1, 4),
(1275, 'ROLE_PROCEDURE_PERFORMANCE_REPORT', 'PERFORMANCE', 127, 'jeremi16us', '2017-10-13 00:00:00', 1, 5),
(1276, 'ROLE_PROCEDURE_PERFORMANCE_REPORT_FOR_DOCTOR_ONLY', 'PERFORMANCE FOR DOCTOR ONLY', 127, 'jeremi16us', '2017-10-13 00:00:00', 1, 6);


##Dialysis Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (128, 'Reports', 'Reports', 12, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1281, 'ROLE_DIALYSIS_REPORT_VIEW', 'VIEW', 128, 'jeremi16us', '2017-10-13 00:00:00', 1, 1),
(1282, 'ROLE_DIALYSIS_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 128, 'jeremi16us', '2017-10-16 00:00:00', 1, 2),
(1283, 'ROLE_DIALYSIS_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 128, 'jeremi16us', '2017-10-16 00:00:00', 1, 3),
(1284, 'ROLE_DIALYSIS_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 128, 'jeremi16us', '2017-10-16 00:00:00', 1, 4),
(1285, 'ROLE_DIALYSIS_PERFORMANCE_REPORT', 'PERFORMANCE', 128, 'jeremi16us', '2017-10-16 00:00:00', 1, 5);

##Optical Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (129, 'Reports', 'Reports', 20, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1291, 'ROLE_OPTICAL_REPORT_VIEW', 'VIEW', 129, 'jeremi16us', '2017-10-13 00:00:00', 1, 1),
 (1292, 'ROLE_OPTICAL_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 129, 'jeremi16us', '2017-10-16 00:00:00', 1, 2),
(1293, 'ROLE_OPTICAL_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 129, 'jeremi16us', '2017-10-16 00:00:00', 1, 3),
(1294, 'ROLE_OPTICAL_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 129, 'jeremi16us', '2017-10-16 00:00:00', 1, 4),
(1295, 'ROLE_OPTICAL_PERFORMANCE_REPORT', 'PERFORMANCE', 129, 'jeremi16us', '2017-10-16 00:00:00', 1, 5);

##Pharmacy Works Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (130, 'Reports', 'Reports', 8, 'jeremi16us', '2016-12-07 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1301, 'ROLE_PHARMACY_REPORT_VIEW', 'VIEW', 130, 'jeremi16us', '2017-10-13 00:00:00', 1, 1),
(1302, 'ROLE_PHARMACY_NUMBER_OF_TESTS_REPORT', 'NUMBER OF TESTS', 130, 'jeremi16us', '2017-10-16 00:00:00', 1, 2),
(1303, 'ROLE_PHARMACY_PATIENT_INVESTIGATION_REPORT', 'PATIENT INVESTIGATION', 130, 'jeremi16us', '2017-10-16 00:00:00', 1, 3),
(1304, 'ROLE_PHARMACY_REVENUE_COLLECTION_REPORT', 'REVENUE COLLECTION', 130, 'jeremi16us', '2017-10-16 00:00:00', 1, 4),
(1305, 'ROLE_PHARMACY_PERFORMANCE_REPORT', 'PERFORMANCE', 130, 'jeremi16us', '2017-10-16 00:00:00', 1, 5),
(1306, 'ROLE_PHARMACY_MEDICATION_ADDED_BY_PHARMACIST_REPORT', 'MEDICATION ADDED BY PHARMACIST REPORT', 130, 'jeremi16us', '2023-04-29 00:00:00', 1, 6);


#Doctor Report
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (131, 'Reports', 'Reports', 11, 'jeremi16us', '2017-10-25 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1311, 'ROLE_DOCTOR_WARD_ROUND_REPORT', 'DOCTOR WARD ROUND REPORT', 131, 'jeremi16us', '2017-10-25 00:00:00', 1, 1),
       (1312, 'ROLE_DOCTOR_CONSULTATION_REPORT', 'DOCTOR CONSULTATION REPORT', 131, 'jeremi16us', '2017-10-25 00:00:00', 1, 2),
       (1313, 'ROLE_DOCTOR_WARD_ROUND_REPORT_FOR_DOCTOR_ONLY', 'DOCTOR WARD ROUND REPORT FOR DOCTOR', 131, 'jeremi16us', '2017-10-25 00:00:00', 1, 3),
       (1314, 'ROLE_DOCTOR_CONSULTATION_REPORT_FOR_DOCTOR_ONLY', 'DOCTOR CONSULTATION REPORT FOR DOCTOR', 131, 'jeremi16us', '2017-10-25 00:00:00', 1, 4);

##Management Reports
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (141, 'Reports', 'Reports', 17, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1411, 'ROLE_MANAGEMENT_REPORT_DAILY_SUMMARY', 'DAILY SUMMARY', 141, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
(1412, 'ROLE_MANAGEMENT_REPORT_WEEKLY_SUMMARY', 'WEEKLY SUMMARY', 141, 'jeremi16us', '2017-06-28 00:00:00', 1, 0);

#Patient Baby Care
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (142, 'Patient Baby Care', 'Patient Baby Care', 10, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1421, 'ROLE_PATIENT_BABY_CARE_VIEW', 'VIEW', 142, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1422, 'ROLE_PATIENT_BABY_CARE_EDIT', 'EDIT', 142, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1423, 'ROLE_PATIENT_BABY_CARE_CREATE', 'CREATE', 142, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1424, 'ROLE_PATIENT_BABY_CARE_DELETE', 'DELETE', 142, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);


#Patient Labour Ward Nurse Notes
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (132, 'Patient Labour Ward Nurse Notes', 'Patient Labour Ward Nurse Notes', 10, 'jeremi16us', '2017-11-29 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1321, 'ROLE_PATIENT_LABOUR_WARD_NURSE_NOTES_VIEW', 'VIEW', 132, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1322, 'ROLE_PATIENT_LABOUR_WARD_NURSE_NOTES_EDIT', 'EDIT', 132, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1323, 'ROLE_PATIENT_LABOUR_WARD_NURSE_NOTES_CREATE', 'CREATE', 132, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1324, 'ROLE_PATIENT_LABOUR_WARD_NURSE_NOTES_DELETE', 'DELETE', 132, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

#Patient Past Medical History
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (136, 'Patient Past Medical History', 'Patient Past Medical History', 11, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1361, 'ROLE_PATIENT_PAST_MEDICAL_HISTORY_VIEW', 'VIEW', 136, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1362, 'ROLE_PATIENT_PAST_MEDICAL_HISTORY_EDIT', 'EDIT', 136, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1363, 'ROLE_PATIENT_PAST_MEDICAL_HISTORY_CREATE', 'CREATE', 136, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1364, 'ROLE_PATIENT_PAST_MEDICAL_HISTORY_DELETE', 'DELETE', 136, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

#Patient Paediatric History
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (137, 'Patient Paediatric History', 'Patient Paediatric History', 11, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1371, 'ROLE_PATIENT_PAEDIATRIC_HISTORY_VIEW', 'VIEW', 137, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1372, 'ROLE_PATIENT_PAEDIATRIC_HISTORY_EDIT', 'EDIT', 137, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1373, 'ROLE_PATIENT_PAEDIATRIC_HISTORY_CREATE', 'CREATE', 137, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1374, 'ROLE_PATIENT_PAEDIATRIC_HISTORY_DELETE', 'DELETE', 137, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

#Patient Family Social History
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (138, 'Patient Family Social History', 'Patient Family Social History', 11, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1381, 'ROLE_PATIENT_FAMILY_SOCIAL_HISTORY_VIEW', 'VIEW', 138, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1382, 'ROLE_PATIENT_FAMILY_SOCIAL_HISTORY_EDIT', 'EDIT', 138, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1383, 'ROLE_PATIENT_FAMILY_SOCIAL_HISTORY_CREATE', 'CREATE', 138, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1384, 'ROLE_PATIENT_FAMILY_SOCIAL_HISTORY_DELETE', 'DELETE', 138, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (139, 'Directory', 'Directory', 22, 'jeremi16us', '2017-12-20 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1391, 'ROLE_DIRECTORY_VIEW', 'VIEW', 139, 'jeremi16us', '2017-12-20 00:00:00', 1, 0),
(1392, 'ROLE_DIRECTORY_CREATE', 'CREATE', 139, 'jeremi16us', '2017-12-20 00:00:00', 1, 0),
(1393, 'ROLE_DIRECTORY_EDIT', 'EDIT', 139, 'jeremi16us', '2017-12-20 00:00:00', 1, 0),
(1394, 'ROLE_DIRECTORY_DELETE', 'DELETE', 139, 'jeremi16us', '2017-12-20 00:00:00', 1, 0);

#2018/01/09 --
################
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (140, 'Employee Type', 'Employee Type', 23, 'jeremi16us', '2018-01-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1401, 'ROLE_DOCTOR', ' DOCTOR', 140, 'jeremi16us', '2018-01-09 00:00:00', 1, 0),
(1402, 'ROLE_NURSE', ' NURSE', 140, 'jeremi16us', '2018-01-09 00:00:00', 1, 0),
(1403, 'ROLE_SURGEON', ' SURGEON', 140, 'jeremi16us', '2018-01-09 00:00:00', 1, 0),
(1404, 'ROLE_SCRUB_NURSE', ' SCRUB NURSE', 140, 'jeremi16us', '2018-01-09 00:00:00', 1, 0),
(1405, 'ROLE_ANAESTHETIC', ' ANAESTHETIC', 140, 'jeremi16us', '2018-01-09 00:00:00', 1, 0),
(1406, 'ROLE_PHARMACY', ' PHARMACY', 140, 'jeremi16us', '2018-01-09 00:00:00', 1, 0),
(1407, 'ROLE_ACC_REV', ' ACC REV', 140, 'jeremi16us', '2022-07-22 00:00:00', 1, 0);


#Patient Excuse of Duty
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (150, 'Patient Excuse of Duty', 'Patient Excuse of Duty', 11, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1501, 'ROLE_PATIENT_EXCUSE_DUTY_VIEW', 'VIEW', 150, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1502, 'ROLE_PATIENT_EXCUSE_DUTY_EDIT', 'EDIT', 150, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1503, 'ROLE_PATIENT_EXCUSE_DUTY_CREATE', 'CREATE', 150, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1504, 'ROLE_PATIENT_EXCUSE_DUTY_DELETE', 'DELETE', 150, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1505, 'ROLE_PATIENT_EXCUSE_DUTY_CAN_PRINT', 'PRINT', 150, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1506, 'ROLE_PATIENT_EXCUSE_DUTY_CAN_APPROVE', 'APPROVE', 150, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

# Active User Login
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (151, 'Active User Login', 'Active User Login ', 2, 'jeremi16us', '2017-06-01 00:00:00', 1, 32);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1511, 'ROLE_ACTIVE_USER_LOGIN_VIEW', 'VIEW', 151, 'jeremi16us', '2017-06-01 00:00:00', 1, 0),
       (1512, 'ROLE_ACTIVE_USER_LOGIN_LOGOUT_EMPLOYEE', 'LOGOUT EMPLOYEE', 151, 'jeremi16us', '2017-06-01 00:00:00', 1, 0);

#Patient Check In ACTION
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (152, 'Patient CheckIn Action', 'Patient CheckIn Action', 11, 'jeremi16us', '2017-08-09 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1521, 'ROLE_PATIENT_CHECK_IN_NO_SHOW', 'NO SHOW', 152, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1522, 'ROLE_PATIENT_CHECK_IN_SIGN_OFF', 'SIGN OFF', 152, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1523, 'ROLE_PATIENT_CHECK_IN_TRANSFER', 'TRANSFER', 152, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1524, 'ROLE_PATIENT_CHECK_IN_CANCEL_SIGN_OFF', 'CANCEL SIGN OFF', 152, 'jeremi16us', '2017-08-09 00:00:00', 1, 0),
       (1525, 'ROLE_PATIENT_CHECK_IN_CANCEL_NO_SHOW', 'CANCEL NO SHOW', 152, 'jeremi16us', '2017-08-09 00:00:00', 1, 0);

##Pharmacy Works Purchase Order
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (153, 'Current Outpatient', 'Current Outpatient', 8, 'jeremi16us', '2017-10-12 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1531, 'ROLE_PHARMACY_CURRENT_OUTPATIENT_VIEW', 'VIEW', 153, 'jeremi16us', '2017-10-12 00:00:00', 1, 0),
(1532, 'ROLE_PHARMACY_CURRENT_OUTPATIENT_TRANSACT', 'TRANSACT', 153, 'jeremi16us', '2017-10-12 00:00:00', 1, 0);

##Debtors Management
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (154, 'Debtors Management', 'Debtors Management', 24, 'jeremi16us', '2019-03-18 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1541, 'ROLE_DEBTORS_MANAGEMENT_VIEW', 'VIEW', 154, 'jeremi16us', '2019-03-18 00:00:00', 1, 0);

##Debtors Management
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (155, 'Debt', 'Debt', 24, 'jeremi16us', '2019-03-18 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1551, 'ROLE_DEBT_VIEW', 'VIEW', 155, 'jeremi16us', '2019-03-18 00:00:00', 1, 0),
(1552, 'ROLE_DEBT_CREATE', 'CREATE', 155, 'jeremi16us', '2019-03-18 00:00:00', 1, 0),
(1553, 'ROLE_DEBT_EDIT', 'EDIT', 155, 'jeremi16us', '2019-03-18 00:00:00', 1, 0),
(1554, 'ROLE_DEBT_DELETE', 'DELETE', 155, 'jeremi16us', '2019-03-18 00:00:00', 1, 0);

#Patient Discharge Summary
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (156, 'Patient Discharge Summary', 'Patient Discharge Summary', 11, 'jeremi16us', '2019-03-20 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1561, 'ROLE_PATIENT_DISCHARGE_SUMMARY_VIEW', 'VIEW', 156, 'jeremi16us', '2019-03-20 00:00:00', 1, 0),
       (1562, 'ROLE_PATIENT_DISCHARGE_SUMMARY_EDIT', 'EDIT', 156, 'jeremi16us', '2019-03-20 00:00:00', 1, 0),
       (1563, 'ROLE_PATIENT_DISCHARGE_SUMMARY_CREATE', 'CREATE', 156, 'jeremi16us', '2019-03-20 00:00:00', 1, 0),
       (1564, 'ROLE_PATIENT_DISCHARGE_SUMMARY_DELETE', 'DELETE', 156, 'jeremi16us', '2019-03-20 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (157, 'Sponsor Pre-Approval Item', 'Sponsor Pre-Approval Item', 2, 'jeremi16us', '2019-04-17 00:00:00', 1, 25);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1571, 'ROLE_ITEMS_SPONSOR_PRE_APPROVAL_ITEMS_VIEW', 'VIEW', 157, 'jeremi16us', '2019-04-17 00:00:00', 1, 0),
(1572, 'ROLE_ITEMS_SPONSOR_PRE_APPROVAL_ITEMS_ADD_REMOVE', 'ADD OR REMOVE', 157, 'jeremi16us', '2019-04-17 00:00:00', 1, 0);

#Claim Processing
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (158, 'Claim Processing', 'Claim Processing', 25, 'jeremi16us', '2019-04-29 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1581, 'ROLE_CLAIM_PROCESSING_VIEW', 'VIEW', 158, 'jeremi16us', '2019-04-29 00:00:00', 1, 0),
       (1582, 'ROLE_CLAIM_PROCESSING_PROCESS_CLAIM', 'PROCESS CLAIM', 158, 'jeremi16us', '2019-04-29 00:00:00', 1, 0);

#Claim Processing Setup
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (159, 'Claim Processing Setup', 'Claim Processing Setup', 25, 'jeremi16us', '2019-04-29 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1591, 'ROLE_CLAIM_PROCESSING_SETUP_VIEW', 'VIEW', 159, 'jeremi16us', '2019-04-29 00:00:00', 1, 0),
       (1592, 'ROLE_CLAIM_PROCESSING_SETUP_ASSIGN_EMPLOYEE', 'ASSIGN EMPLOYEE', 159, 'jeremi16us', '2019-04-29 00:00:00', 1, 0),
       (1593, 'ROLE_CLAIM_PROCESSING_SETUP_MAP_ITEMS', 'MAP ITEMS', 159, 'jeremi16us', '2019-04-29 00:00:00', 1, 0);

#Patient Check In
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (160, 'Patient Check In', 'Patient Check In', 25, 'jeremi16us', '2019-05-04 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1601, 'ROLE_PATIENT_CHECK_IN_VIEW', 'VIEW', 160, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1602, 'ROLE_PATIENT_CHECK_IN_CREATE', 'CREATE', 160, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1603, 'ROLE_PATIENT_CHECK_IN_EDIT', 'EDIT', 160, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1604, 'ROLE_PATIENT_CHECK_IN_DELETE', 'DELETE', 160, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1605, 'ROLE_PATIENT_CHECK_IN_EDIT_AUTH', 'EDIT AUTH NO', 160, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1606, 'ROLE_PATIENT_CHECK_IN_EDIT_FOLIO', 'EDIT FOLIO', 160, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1607, 'ROLE_PATIENT_CHECK_IN_EDIT_DETAILS', 'DETAILS', 160, 'jeremi16us', '2023-06-14 00:00:00', 1, 0),
       (1608, 'ROLE_PATIENT_CHECK_IN_TRACK_CLAIM_PROCESS', 'TRACK CLAIM PROCESS', 160, 'jeremi16us', '2023-06-14 00:00:00', 1, 0),
       (1609, 'ROLE_PATIENT_CHECK_IN_MORE_ACTION', 'MORE ACTION', 160, 'jeremi16us', '2023-06-14 00:00:00', 1, 0);

#Message
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (161, 'Message', 'Message', 26, 'jeremi16us', '2019-05-04 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1611, 'ROLE_MESSAGE_VIEW', 'VIEW', 161, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1612, 'ROLE_MESSAGE_CREATE', 'CREATE', 161, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1613, 'ROLE_MESSAGE_EDIT', 'EDIT', 161, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1614, 'ROLE_MESSAGE_DELETE', 'DELETE', 161, 'jeremi16us', '2019-05-04 00:00:00', 1, 0);

#Claim Batch
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (162, 'Claim Batch', 'Claim Batch', 25, 'jeremi16us', '2019-05-04 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1621, 'ROLE_CLAIM_BATCH_VIEW', 'VIEW', 162, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1622, 'ROLE_CLAIM_BATCH_CREATE', 'CREATE', 162, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1623, 'ROLE_CLAIM_BATCH_EDIT', 'EDIT', 162, 'jeremi16us', '2019-05-04 00:00:00', 1, 0),
       (1624, 'ROLE_CLAIM_BATCH_DELETE', 'DELETE', 162, 'jeremi16us', '2019-05-04 00:00:00', 1, 0);

#Claim Item
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (163, 'Claim Item', 'Claim Item', 25, 'jeremi16us', '2019-05-14 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1631, 'ROLE_CLAIM_ITEM_VIEW', 'VIEW', 163, 'jeremi16us', '2019-05-14 00:00:00', 1, 0),
       (1632, 'ROLE_CLAIM_ITEM_CREATE', 'CREATE', 163, 'jeremi16us', '2019-05-14 00:00:00', 1, 0),
       (1633, 'ROLE_CLAIM_ITEM_EDIT', 'EDIT', 163, 'jeremi16us', '2019-05-14 00:00:00', 1, 0),
       (1634, 'ROLE_CLAIM_ITEM_DELETE', 'DELETE', 163, 'jeremi16us', '2019-05-14 00:00:00', 1, 0);

#NHIF ECLAIM
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (164, 'NHIF ECLAIM', 'NHIF eCLaim', 25, 'jeremi16us', '2019-04-29 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1641, 'ROLE_NHIF_ECLAIM_VIEW', 'VIEW', 164, 'jeremi16us', '2019-04-29 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (165, 'Item Report', 'Item Report', 2, 'jeremi16us', '2019-10-13 00:00:00', 1, 165);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1651, 'ROLE_ITEM_REPORT_VIEW', 'VIEW', 165, 'jeremi16us', '2019-10-13 00:00:00', 1, 0),
(1652, 'ROLE_ITEM_REPORT_CREATE', 'CREATE', 165, 'jeremi16us', '2019-10-13 00:00:00', 1, 0),
(1653, 'ROLE_ITEM_REPORT_EDIT', 'EDIT', 165, 'jeremi16us', '2019-10-13 00:00:00', 1, 0),
(1654, 'ROLE_ITEM_REPORT_DELETE', 'DELETE', 165, 'jeremi16us', '2019-10-13 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (166, 'Item Report Template', 'Item Report Template', 2, 'jeremi16us', '2019-10-13 00:00:00', 1, 166);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1661, 'ROLE_ITEM_REPORT_TEMPLATE_VIEW', 'VIEW', 166, 'jeremi16us', '2019-10-13 00:00:00', 1, 0),
(1662, 'ROLE_ITEM_REPORT_TEMPLATE_CREATE', 'CREATE', 166, 'jeremi16us', '2019-10-13 00:00:00', 1, 0),
(1663, 'ROLE_ITEM_REPORT_TEMPLATE_EDIT', 'EDIT', 166, 'jeremi16us', '2019-10-13 00:00:00', 1, 0),
(1664, 'ROLE_ITEM_REPORT_TEMPLATE_DELETE', 'DELETE', 166, 'jeremi16us', '2019-10-13 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (167, 'Scheduler', 'Scheduler', 2, 'jeremi16us', '2019-10-17 00:00:00', 1, 167);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1671, 'ROLE_SCHEDULER_VIEW', 'VIEW', 167, 'jeremi16us', '2019-10-17 00:00:00', 1, 0),
(1672, 'ROLE_SCHEDULER_CREATE', 'CREATE', 167, 'jeremi16us', '2019-10-17 00:00:00', 1, 0),
(1673, 'ROLE_SCHEDULER_EDIT', 'EDIT', 167, 'jeremi16us', '2019-10-17 00:00:00', 1, 0),
(1674, 'ROLE_SCHEDULER_DELETE', 'DELETE', 167, 'jeremi16us', '2019-10-17 00:00:00', 1, 0);

##Patient Record Actions
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (168, 'Patient Record Actions', 'Patient Record Actions', 16, 'jeremi16us', '2019-11-02 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1681, 'ROLE_PT_ACTIONS_EDIT_OUTPATIENT_CONSULTATION', 'EDIT OUTPATIENT CONSULTATION', 168, 'jeremi16us', '2019-11-02 00:00:00', 1, 0),
       (1682, 'ROLE_PT_ACTIONS_EDIT_INPATIENT_WARD_ROUND', 'EDIT INPATIENT WARD ROUND', 168, 'jeremi16us', '2019-11-02 00:00:00', 1, 0),
       (1683, 'ROLE_PT_ACTIONS_DELAY_REQUEST', 'REQUEST', 168, 'jeremi16us', '2019-11-11 00:00:00', 1, 0),
       (1684, 'ROLE_PT_ACTIONS_OTHER_ORDER', 'VIEW OTHER ORDER', 168, 'jeremi16us', '2019-11-13 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (169, 'Order Item', 'Order Item', 16, 'jeremi16us', '2019-11-05 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1691, 'ROLE_PATIENT_ORDER_ITEM_VIEW', 'VIEW', 169, 'jeremi16us', '2019-11-05 00:00:00', 1, 0),
(1692, 'ROLE_PATIENT_ORDER_ITEM_CREATE', 'CREATE', 169, 'jeremi16us', '2019-11-05 00:00:00', 1, 0),
(1693, 'ROLE_PATIENT_ORDER_ITEM_EDIT', 'EDIT', 169, 'jeremi16us', '2019-11-05 00:00:00', 1, 0),
(1694, 'ROLE_PATIENT_ORDER_ITEM_DELETE', 'DELETE', 169, 'jeremi16us', '2019-11-05 00:00:00', 1, 0);

##File Summary
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (170, 'File Summary', 'File Summary', 16, 'jeremi16us', '2017-06-28 00:00:00', 1, 1);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1701, 'ROLE_FILE_SUMMARY_VIEW', 'VIEW SUMMARY', 170, 'jeremi16us', '2017-06-28 00:00:00', 1, 0),
       (1702, 'ROLE_FILE_SUMMARY_VIEW_DETAILS', 'VIEW DETAILS', 170, 'jeremi16us', '2018-01-03 00:00:00', 1, 0);

#Patient Anaesthesia Record
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (171, 'Patient Anaesthesia Record', 'Patient Anaesthesia Record', 14, 'jeremi16us', '2020-04-13 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1711, 'ROLE_PATIENT_ANAESTHESIA_RECORD_VIEW', 'VIEW', 171, 'jeremi16us', '2020-04-13 00:00:00', 1, 0),
       (1712, 'ROLE_PATIENT_ANAESTHESIA_RECORD_EDIT', 'EDIT', 171, 'jeremi16us', '2020-04-13 00:00:00', 1, 0),
       (1713, 'ROLE_PATIENT_ANAESTHESIA_RECORD_CREATE', 'CREATE', 171, 'jeremi16us', '2020-04-13 00:00:00', 1, 0),
       (1714, 'ROLE_PATIENT_ANAESTHESIA_RECORD_DELETE', 'DELETE', 171, 'jeremi16us', '2020-04-13 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (172, 'System Notification', 'System Notification', 2, 'jeremi16us', '2020-06-02 00:00:00', 1, 0);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1721, 'ROLE_SYSTEM_NOTIFICATION_VIEW', 'VIEW', 172, 'jeremi16us', '2020-06-02 00:00:00', 1, 0),
(1722, 'ROLE_SYSTEM_NOTIFICATION_CREATE', 'CREATE', 172, 'jeremi16us', '2020-06-02 00:00:00', 1, 0),
(1723, 'ROLE_SYSTEM_NOTIFICATION_EDIT', 'EDIT', 172, 'jeremi16us', '2020-06-02 00:00:00', 1, 0),
(1724, 'ROLE_SYSTEM_NOTIFICATION_DELETE', 'DELETE', 172, 'jeremi16us', '2020-06-02 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (173, 'Patient Insurer', 'Patient Insurer', 3, 'jeremi16us', '2020-11-18 00:00:00', 1, 4);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1731, 'ROLE_PATIENT_INSURER_VIEW', 'VIEW', 173, 'jeremi16us', '2020-11-18 00:00:00', 1, 0),
(1732, 'ROLE_PATIENT_INSURER_CREATE', 'CREATE', 173, 'jeremi16us', '2020-11-18 00:00:00', 1, 0),
(1733, 'ROLE_PATIENT_INSURER_EDIT', 'EDIT', 173, 'jeremi16us', '2020-11-18 00:00:00', 1, 0),
(1734, 'ROLE_PATIENT_INSURER_DELETE', 'DELETE', 173, 'jeremi16us', '2020-11-18 00:00:00', 1, 0);

#Death Certificate
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (174, 'Death Certificate', 'Death Certificate', 11, 'jeremi16us', '2021-02-17 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1741, 'ROLE_PATIENT_DEATH_CERTIFICATE_VIEW', 'VIEW', 174, 'jeremi16us', '2021-02-17 00:00:00', 1, 0),
       (1742, 'ROLE_PATIENT_DEATH_CERTIFICATE_EDIT', 'EDIT', 174, 'jeremi16us', '2021-02-17 00:00:00', 1, 0),
       (1743, 'ROLE_PATIENT_DEATH_CERTIFICATE_CREATE', 'CREATE', 174, 'jeremi16us', '2021-02-17 00:00:00', 1, 0),
       (1744, 'ROLE_PATIENT_DEATH_CERTIFICATE_DELETE', 'DELETE', 174, 'jeremi16us', '2021-02-17 00:00:00', 1, 0);

#Death Remark
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (175, 'Death Remark', 'Death Remark', 11, 'jeremi16us', '2021-02-17 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1751, 'ROLE_PATIENT_DEATH_REMARK_VIEW', 'VIEW', 175, 'jeremi16us', '2021-02-17 00:00:00', 1, 0),
       (1752, 'ROLE_PATIENT_DEATH_REMARK_EDIT', 'EDIT', 175, 'jeremi16us', '2021-02-17 00:00:00', 1, 0),
       (1753, 'ROLE_PATIENT_DEATH_REMARK_CREATE', 'CREATE', 175, 'jeremi16us', '2021-02-17 00:00:00', 1, 0),
       (1754, 'ROLE_PATIENT_DEATH_REMARK_DELETE', 'DELETE', 175, 'jeremi16us', '2021-02-17 00:00:00', 1, 0);

#Patient Appointment
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (176, 'Patient Appointment', 'Patient Appointment', 10, 'jeremi16us', '2021-04-22 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1761, 'ROLE_PATIENT_APPOINTMENT_VIEW', 'VIEW', 176, 'jeremi16us', '2021-04-22 00:00:00', 1, 0),
       (1762, 'ROLE_PATIENT_APPOINTMENT_EDIT', 'EDIT', 176, 'jeremi16us', '2021-04-22 00:00:00', 1, 0),
       (1763, 'ROLE_PATIENT_APPOINTMENT_CREATE', 'CREATE', 176, 'jeremi16us', '2021-04-22 00:00:00', 1, 0),
       (1764, 'ROLE_PATIENT_APPOINTMENT_DELETE', 'DELETE', 176, 'jeremi16us', '2021-04-22 00:00:00', 1, 0);

#Home Care
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (177, 'Home care', 'Home care', 27, 'jeremi16us', '2021-05-15 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1771, 'ROLE_HOME_CARE_VIEW', 'VIEW', 177, 'jeremi16us', '2021-05-15 00:00:00', 1, 0),
       (1773, 'ROLE_HOME_CARE_CREATE', 'CREATE', 177, 'jeremi16us', '2021-05-15 00:00:00', 1, 0);

##Store Works Stock Room
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (178, 'Stock Room', 'Stock Room', 6, 'jeremi16us', '2020-08-19 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1781, 'ROLE_STOCK_ROOM_VIEW', 'VIEW', 178, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1782, 'ROLE_STOCK_ROOM_CREATE', 'CREATE', 178, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1783, 'ROLE_STOCK_ROOM_EDIT', 'EDIT', 178, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1784, 'ROLE_STOCK_ROOM_CANCEL', 'CANCEL', 178, 'jeremi16us', '2020-08-19 00:00:00', 1, 0);

##Store Works Stock Room Shelf
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (179, 'Stock Room Shelf', 'Stock Room Shelf', 6, 'jeremi16us', '2020-08-19 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1791, 'ROLE_STOCK_ROOM_SHELF_VIEW', 'VIEW', 179, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1792, 'ROLE_STOCK_ROOM_SHELF_CREATE', 'CREATE', 179, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1793, 'ROLE_STOCK_ROOM_SHELF_EDIT', 'EDIT', 179, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1794, 'ROLE_STOCK_ROOM_SHELF_CANCEL', 'CANCEL', 179, 'jeremi16us', '2020-08-19 00:00:00', 1, 0);

##Store Works Stock Count
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (180, 'Stock Count', 'Stock Count', 6, 'jeremi16us', '2020-08-19 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1801, 'ROLE_STOCK_COUNT_VIEW', 'VIEW', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1802, 'ROLE_STOCK_COUNT_CREATE', 'CREATE', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1803, 'ROLE_STOCK_COUNT_EDIT', 'EDIT', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1804, 'ROLE_STOCK_COUNT_CANCEL', 'CANCEL', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1805, 'ROLE_STOCK_COUNT_ITEM_REMOVE', 'REMOVE ITEM', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1806, 'ROLE_STOCK_COUNT_SUPERVISION', 'SUPERVISION', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0),
(1807, 'ROLE_STOCK_COUNT_APPROVAL', 'APPROVAL', 180, 'jeremi16us', '2020-08-19 00:00:00', 1, 0);

##payment types
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (181, 'Payment Type', 'Payment Type', 2, 'Festus', '2023-01-20 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1811, 'ROLE_PAYMENT_TYPE_CREATE', 'CREATE', 181, 'Festus', '2023-01-19 00:00:00', 1, 0),
(1812, 'ROLE_PAYMENT_TYPE_EDIT', 'EDIT', 181, 'Festus', '2023-01-20 00:00:00', 1, 0),
(1813, 'ROLE_PAYMENT_TYPES_VIEW', 'VIEW', 181, 'Festus', '2023-01-20 00:00:00', 1, 0),
(1814, 'ROLE_PAYMENT_TYPE_DELETE', 'DELETE', 181, 'Festus', '2023-01-20 00:00:00', 1, 0);


##payment accounts
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (182, 'Payment Accounts', 'Payment Accounts', 2, 'Festus', '2023-01-20 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1821, 'ROLE_PAYMENT_ACCOUNT_CREATE', 'CREATE', 182, 'Festus', '2023-01-19 00:00:00', 1, 0),
(1822, 'ROLE_PAYMENT_ACCOUNT_EDIT', 'EDIT', 182, 'Festus', '2023-01-20 00:00:00', 1, 0),
(1823, 'ROLE_PAYMENT_ACCOUNT_VIEW', 'VIEW', 182, 'Festus', '2023-01-20 00:00:00', 1, 0),
(1824, 'ROLE_PAYMENT_ACCOUNT_DELETE', 'DELETE', 182, 'Festus', '2023-01-20 00:00:00', 1, 0);


##Employee performance
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (183, 'Employee Performance', 'Employee Performance', 23, 'Festus', '2023-01-20 00:00:00', 1, 183);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1831, 'ROLE_EMPLOYEE_PERFORMANCE_REPORT_VIEW', 'VIEW', 183, 'Festus', '2016-12-06 00:00:00', 1, 0);


##Document Management
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (184, 'Document Management', 'Document Management', 28, 'rugemarila', '2023-02-23 00:00:00', 1, 184);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1841, 'ROLE_DOCUMENT_MANAGEMENT_VIEW', 'VIEW', 184, 'rugemarila', '2023-02-23 00:00:00', 1, 0);

##Document Type
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (185, 'Document Type', 'Document Type', 28, 'rugemarila', '2023-02-23 00:00:00', 1, 185);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1851, 'ROLE_DOCUMENT_TYPE_CREATE', 'CREATE', 185, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1852, 'ROLE_DOCUMENT_TYPE_EDIT', 'EDIT', 185, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1853, 'ROLE_DOCUMENT_TYPE_VIEW', 'VIEW', 185, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1854, 'ROLE_DOCUMENT_TYPE_DELETE', 'DELETE', 185, 'rugemarila', '2023-02-23 00:00:00', 1, 0);

##Document
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (186, 'Document', 'Document', 28, 'rugemarila', '2023-02-23 00:00:00', 1, 186);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1861, 'ROLE_DOCUMENT_CREATE', 'CREATE', 186, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1862, 'ROLE_DOCUMENT_EDIT', 'EDIT', 186, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1863, 'ROLE_DOCUMENT_VIEW', 'VIEW', 186, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1864, 'ROLE_DOCUMENT_DELETE', 'DELETE', 186, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1865, 'ROLE_DOCUMENT_ADD_TO_PATIENT', 'ADD TO PATIENT', 186, 'rugemarila', '2023-02-23 00:00:00', 1, 0),
        (1866, 'ROLE_DOCUMENT_ADD_TO_EMPLOYEE', 'ADD TO EMPLOYEE', 186, 'rugemarila', '2023-02-23 00:00:00', 1, 0);

##Accounting
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (187, 'Accounting', 'Accounting', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 187);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1871, 'ROLE_ACCOUNTING_VIEW', 'VIEW', 187, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Chart of Accounts
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (188, 'Chart Of Accounts', 'Chart Of Accounts', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 188);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1881, 'ROLE_CHART_OF_ACCOUNTS_CREATE', 'CREATE', 188, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (1882, 'ROLE_CHART_OF_ACCOUNTS_EDIT', 'EDIT', 188, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (1883, 'ROLE_CHART_OF_ACCOUNTS_VIEW', 'VIEW', 188, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (1884, 'ROLE_CHART_OF_ACCOUNTS_DELETE', 'DELETE', 188, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Purchase Invoice
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (450, 'Purchase Invoice', 'Purchase Invoice', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 450);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (4501, 'ROLE_PURCHASE_INVOICE_CREATE', 'CREATE', 450, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4502, 'ROLE_PURCHASE_INVOICE_EDIT', 'EDIT', 450, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4503, 'ROLE_PURCHASE_INVOICE_VIEW', 'VIEW', 450, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4504, 'ROLE_PURCHASE_INVOICE_PREVIEW', 'PREVIEW PURCHASE INVOICE', 450, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4505, 'ROLE_PURCHASE_INVOICE_DELETE', 'DELETE', 450, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4506, 'ROLE_PURCHASE_INVOICE_FROM_PURCHASE_ORDER', 'GENERATE PURCHASE INVOICE FROM PURCHASE ORDER', 450, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Sales Invoice
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (451, 'Sales Invoice', 'Purchase Invoice', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 451);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (4511, 'ROLE_SALES_INVOICE_CREATE', 'CREATE', 451, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4512, 'ROLE_SALES_INVOICE_EDIT', 'EDIT', 451, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4513, 'ROLE_SALES_INVOICE_VIEW', 'VIEW', 451, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4514, 'ROLE_SALES_INVOICE_PREVIEW', 'PREVIEW SALES INVOICE', 451, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4515, 'ROLE_SALES_INVOICE_DELETE', 'DELETE', 451, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##PaymentEntry
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (452, 'Payment Entry', 'Payment Entry', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 452);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (4521, 'ROLE_PAYMENT_ENTRY_CREATE', 'CREATE', 452, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4522, 'ROLE_PAYMENT_ENTRY_EDIT', 'EDIT', 452, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4523, 'ROLE_PAYMENT_ENTRY_VIEW', 'VIEW', 452, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4524, 'ROLE_PAYMENT_ENTRY_PREVIEW', 'PREVIEW', 452, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4525, 'ROLE_PAYMENT_ENTRY_DELETE', 'DELETE', 452, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##JournalEntry
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (453, 'Journal Entry', 'Journal Entry', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 453);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (4531, 'ROLE_JOURNAL_ENTRY_CREATE', 'CREATE', 453, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4532, 'ROLE_JOURNAL_ENTRY_EDIT', 'EDIT', 453, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4533, 'ROLE_JOURNAL_ENTRY_VIEW', 'VIEW', 453, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4534, 'ROLE_JOURNAL_ENTRY_PREVIEW', 'PREVIEW', 453, 'rugemarila', '2023-03-02 00:00:00', 1, 0),
        (4535, 'ROLE_JOURNAL_ENTRY_DELETE', 'DELETE', 453, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##General Ledger
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (454, 'General Ledger', 'General Ledger', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 454);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (4543, 'ROLE_GENERAL_LEDGER_VIEW', 'VIEW', 454, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Customer Ledger Summary
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (455, 'Customer Ledger Summary', 'Customer Ledger Summary', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 455);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (4553, 'ROLE_CUSTOMER_LEDGER_SUMMARY_VIEW', 'VIEW', 455, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Supplier Ledger Summary
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (456, 'Supplier Ledger Summary', 'Supplier Ledger Summary', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 456);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (4563, 'ROLE_SUPPLIER_LEDGER_SUMMARY_VIEW', 'VIEW', 456, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Account Receivable
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (457, 'Account Receivable', 'Account Receivable', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 457);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (4573, 'ROLE_ACCOUNT_RECEIVABLE_VIEW', 'VIEW', 457, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Account Payable
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (458, 'Account Payable', 'Account Payable', 29, 'rugemarila', '2023-03-02 00:00:00', 1, 458);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (4583, 'ROLE_ACCOUNT_PAYABLE_VIEW', 'VIEW', 458, 'rugemarila', '2023-03-02 00:00:00', 1, 0);

##Booking Schedules
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (189, 'Booking Schedule', 'Booking Schedule', 30, 'josephkiwia', '2023-05-15 00:00:00', 1, 189);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1885, 'ROLE_BOOKING_SCHEDULE_CREATE', 'CREATE', 189, 'josephkiwia', '2023-05-15 00:00:00', 1, 0),
        (1886, 'ROLE_BOOKING_SCHEDULE_EDIT', 'EDIT', 189, 'josephkiwia', '2023-05-15 00:00:00', 1, 0),
        (1887, 'ROLE_BOOKING_SCHEDULE_VIEW', 'VIEW', 189, 'josephkiwia', '2023-05-15 00:00:00', 1, 0),
        (1888, 'ROLE_BOOKING_SCHEDULE_DELETE', 'DELETE', 189, 'josephkiwia', '2023-05-15 00:00:00', 1, 0);


##Reception Appointments
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (190, 'Reception Appointment', 'Reception Appointment', 30, 'josephkiwia', '2023-05-15 00:00:00', 1, 190);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1889, 'ROLE_RECEPTION_APPOINTMENT_CREATE', 'CREATE', 190, 'josephkiwia', '2023-05-15 00:00:00', 1, 0),
        (1890, 'ROLE_RECEPTION_APPOINTMENT_EDIT', 'EDIT', 190, 'josephkiwia', '2023-05-15 00:00:00', 1, 0),
        (1891, 'ROLE_RECEPTION_APPOINTMENT_VIEW', 'VIEW', 190, 'josephkiwia', '2023-05-15 00:00:00', 1, 0),
        (1892, 'ROLE_RECEPTION_APPOINTMENT_DELETE', 'DELETE', 190, 'josephkiwia', '2023-05-15 00:00:00', 1, 0);

##Reception Appointments
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (191, 'Appointment Calendar', 'Appointment Calendar', 30, 'josephkiwia', '2023-05-15 00:00:00', 1, 191);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES   (1893, 'ROLE_APPOINTMENT_CALENDAR_VIEW', 'VIEW', 191, 'josephkiwia', '2023-05-15 00:00:00', 1, 0);

##Visit Type Configuration
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (192, 'Visit Type Configuration', 'Visit Type Configuration', 2, 'josephkiwia', '2023-05-29 00:00:00', 1, 192);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1894, 'ROLE_VISIT_TYPE_CREATE', 'CREATE', 192, 'josephkiwia', '2023-05-29 00:00:00', 1, 0),
        (1895, 'ROLE_VISIT_TYPE_EDIT', 'EDIT', 192, 'josephkiwia', '2023-05-29 00:00:00', 1, 0),
        (1896, 'ROLE_VISIT_TYPE_VIEW', 'VIEW', 192, 'josephkiwia', '2023-05-29 00:00:00', 1, 0),
        (1897, 'ROLE_VISIT_TYPE_DELETE', 'DELETE', 192, 'josephkiwia', '2023-05-29 00:00:00', 1, 0);

##Visit Type Configuration
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (193, 'Export Files', 'Export Files', 2, 'josephkiwia', '2023-05-29 00:00:00', 1, 193);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1898, 'ROLE_EXPORT_PDF', 'EXPORT PDF', 193, 'josephkiwia', '2023-05-29 00:00:00', 1, 0),
        (1899, 'ROLE_EXPORT_EXCEL', 'EXPORT EXCEL', 193, 'josephkiwia', '2023-05-29 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (194, 'System Version', 'System Version', 2, 'jeremi16us', '2023-06-15 00:00:00', 1, 0);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1941, 'ROLE_SYSTEM_VERSION_VIEW', 'VIEW', 194, 'jeremi16us', '2023-06-15 00:00:00', 1, 0),
        (1942, 'ROLE_SYSTEM_VERSION_CREATE', 'CREATE', 194, 'jeremi16us', '2023-06-15 00:00:00', 1, 0),
        (1943, 'ROLE_SYSTEM_VERSION_EDIT', 'EDIT', 194, 'jeremi16us', '2023-06-15 00:00:00', 1, 0),
        (1944, 'ROLE_SYSTEM_VERSION_DELETE', 'DELETE', 194, 'jeremi16us', '2023-06-15 00:00:00', 1, 0),
        (1945, 'ROLE_SYSTEM_VERSION_BY_PASS_VERSION', 'BY PASS VERSION', 194, 'jeremi16us', '2023-06-15 00:00:00', 1, 0);

##charts
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (195, 'Charts', 'Charts', 2, 'Babuu', '2023-07-03  00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1951, 'ROLE_EMPLOYEE_CHARTS_CREATE', 'CREATE', 195, 'Babuu', '2023-07-03 00:00:00', 1, 0),
(1952, 'ROLE_EMPLOYEE_CHARTS_EDIT', 'EDIT', 195, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1953, 'ROLE_EMPLOYEE_CHARTS_VIEW', 'VIEW', 195, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1954, 'ROLE_EMPLOYEE_CHARTS_DELETE', 'DELETE', 195, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1955, 'ROLE_EMPLOYEE_CHARTS_CUSTOMIZE', 'CUSTOMIZE', 195, 'Babuu', '2023-10-02  00:00:00', 1, 0);

##Charts datasets
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (196, 'Charts Datasets', 'Charts Datasets', 2, 'Babuu', '2023-07-03  00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1961, 'ROLE_DATASETS_CREATE', 'CREATE', 196, 'Babuu', '2023-07-03 00:00:00', 1, 0),
(1962, 'ROLE_DATASETS_EDIT', 'EDIT', 196, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1963, 'ROLE_DATASETS_VIEW', 'VIEW', 196, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1964, 'ROLE_DATASETS_DELETE', 'DELETE', 196, 'Babuu', '2023-07-03  00:00:00', 1, 0);

##Tables Relationship  
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (197, 'Tables Relationship', 'Tables Relationship', 2, 'Babuu', '2023-07-03  00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES (1971, 'ROLE_TABLES_RELATIONSHIP_CREATE', 'CREATE', 197, 'Babuu', '2023-07-03 00:00:00', 1, 0),
(1972, 'ROLE_TABLES_RELATIONSHIP_EDIT', 'EDIT', 197, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1973, 'ROLE_TABLES_RELATIONSHIP_VIEW', 'VIEW', 197, 'Babuu', '2023-07-03  00:00:00', 1, 0),
(1974, 'ROLE_TABLES_RELATIONSHIP_DELETE', 'DELETE', 197, 'Babuu', '2023-07-03  00:00:00', 1, 0);



##Employee Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (200, 'Human Resource', 'Human Resource', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 200);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (1999, 'HUMAN_RESOURCE_VIEW', 'VIEW', 200, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Employee Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (201, 'Employee Details', 'Employee Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 201);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2000, 'ROLE_EMPLOYEE_CREATE', 'CREATE', 201, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2001, 'ROLE_EMPLOYEE_EDIT', 'EDIT', 201, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2002, 'ROLE_EMPLOYEE_VIEW', 'VIEW', 201, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2003, 'ROLE_EMPLOYEE_DELETE', 'DELETE', 201, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

        
##Employee Contacts
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (202, 'Employee Contacts', 'Employee Contacts', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 202);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time,  active, sort)
VALUES  (2004, 'ROLE_EMPLOYEE_CONTACTS_CREATE', 'CREATE', 202, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2005, 'ROLE_EMPLOYEE_CONTACTS_EDIT', 'EDIT', 202, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2006, 'ROLE_EMPLOYEE_CONTACTS_VIEW', 'VIEW', 202, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2007, 'ROLE_EMPLOYEE_CONTACTS_DELETE', 'DELETE', 202, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Employee Emergency Contacts
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (203, 'Employee Emergency Contacts', 'Employee Emergency Contacts', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 203);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2008, 'ROLE_EMPLOYEE_EMERGENCY_CONTACTS_CREATE', 'CREATE', 203, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2009, 'ROLE_EMPLOYEE_EMERGENCY_CONTACTS_EDIT', 'EDIT', 203, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2010, 'ROLE_EMPLOYEE_EMERGENCY_CONTACTS_VIEW', 'VIEW', 203, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2011, 'ROLE_EMPLOYEE_EMERGENCY_CONTACTS_DELETE', 'DELETE', 203, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Employee Dependency Contacts
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (204, 'Employee Dependency Contacts', 'Employee Dependency Contacts', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 204);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2012, 'ROLE_EMPLOYEE_DEPENDENCY_CONTACTS_CREATE', 'CREATE', 204, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2013, 'ROLE_EMPLOYEE_DEPENDENCY_CONTACTS_EDIT', 'EDIT', 204, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2014, 'ROLE_EMPLOYEE_DEPENDENCY_CONTACTS_VIEW', 'VIEW', 204, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2015, 'ROLE_EMPLOYEE_DEPENDENCY_CONTACTS_DELETE', 'DELETE', 204, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Immigration Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (205, 'Employee Immigration Details', 'Employee Immigration Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 205);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2016, 'ROLE_EMPLOYEE_IMMIGRATION_CREATE', 'CREATE', 205, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2017, 'ROLE_EMPLOYEE_IMMIGRATION_EDIT', 'EDIT', 205, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2018, 'ROLE_EMPLOYEE_IMMIGRATION_VIEW', 'VIEW', 205, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2019, 'ROLE_EMPLOYEE_IMMIGRATION_DELETE', 'DELETE', 205, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Joining Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (206, 'Employee Joining Details', 'Employee Joining Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 206);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2020, 'ROLE_EMPLOYEE_JOINING_CREATE', 'CREATE', 206, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2021, 'ROLE_EMPLOYEE_JOINING_EDIT', 'EDIT', 206, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2022, 'ROLE_EMPLOYEE_JOINING_VIEW', 'VIEW', 206, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2023, 'ROLE_EMPLOYEE_JOINING_DELETE', 'DELETE', 206, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Employee Exit Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (207, 'Employee Exit Details', 'Employee Exit Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 207);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2024, 'ROLE_EMPLOYEE_EXIT_CREATE', 'CREATE', 207, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2025, 'ROLE_EMPLOYEE_EXIT_EDIT', 'EDIT', 207, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2026, 'ROLE_EMPLOYEE_EXIT_VIEW', 'VIEW', 207, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2027, 'ROLE_EMPLOYEE_EXIT_DELETE', 'DELETE', 207, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Job Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (208, 'Employee Job Details', 'Employee Job Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 208);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2028, 'ROLE_EMPLOYEE_JOB_CREATE', 'CREATE', 208, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2029, 'ROLE_EMPLOYEE_JOB_EDIT', 'EDIT', 208, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2030, 'ROLE_EMPLOYEE_JOB_VIEW', 'VIEW', 208, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2031, 'ROLE_EMPLOYEE_JOB_DELETE', 'DELETE', 208, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Department Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (209, 'Employee Department Details', 'Employee Department Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 209);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2032, 'ROLE_EMPLOYEE_DEPARTMENT_CREATE', 'CREATE', 209, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2033, 'ROLE_EMPLOYEE_DEPARTMENT_EDIT', 'EDIT', 209, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2034, 'ROLE_EMPLOYEE_DEPARTMENT_VIEW', 'VIEW', 209, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2035, 'ROLE_EMPLOYEE_DEPARTMENT_DELETE', 'DELETE', 209, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Contract Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (210, 'Employee Contract Details', 'Employee Contract Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 210);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2036, 'ROLE_EMPLOYEE_CONTRACT_CREATE', 'CREATE', 210, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2037, 'ROLE_EMPLOYEE_CONTRACT_EDIT', 'EDIT', 210, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2038, 'ROLE_EMPLOYEE_CONTRACT_VIEW', 'VIEW', 210, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2039, 'ROLE_EMPLOYEE_CONTRACT_DELETE', 'DELETE', 210, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

       
##Employee Supervisor Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (211, 'Employee Supervisor Details', 'Employee Supervisor Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 211);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2040, 'ROLE_EMPLOYEE_SUPERVISOR_CREATE', 'CREATE', 211, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2041, 'ROLE_EMPLOYEE_SUPERVISOR_EDIT', 'EDIT', 211, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2042, 'ROLE_EMPLOYEE_SUPERVISOR_VIEW', 'VIEW', 211, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2043, 'ROLE_EMPLOYEE_SUPERVISOR_DELETE', 'DELETE', 211, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

               
##Employee Surbodinate Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (212, 'Employee Surbodinate Details', 'Employee Surbodinate Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 212);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2044, 'ROLE_EMPLOYEE_SURBORDINATE_CREATE', 'CREATE', 212, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2045, 'ROLE_EMPLOYEE_SURBORDINATE_EDIT', 'EDIT', 212, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2046, 'ROLE_EMPLOYEE_SURBORDINATE_VIEW', 'VIEW', 212, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2047, 'ROLE_EMPLOYEE_SURBORDINATE_DELETE', 'DELETE', 212, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

        
               
##Employee Salary Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (213, 'Employee Salary Details', 'Employee Salary Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 213);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2048, 'ROLE_EMPLOYEE_SALARY_CREATE', 'CREATE', 213, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2049, 'ROLE_EMPLOYEE_SALARY_EDIT', 'EDIT', 213, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2050, 'ROLE_EMPLOYEE_SALARY_VIEW', 'VIEW', 213, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2051, 'ROLE_EMPLOYEE_SALARY_DELETE', 'DELETE', 213, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


       
##Employee Membership Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (214, 'Employee Membership Details', 'Employee Membership Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 214);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2052, 'ROLE_EMPLOYEE_MEMBERSHIP_CREATE', 'CREATE', 214, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2053, 'ROLE_EMPLOYEE_MEMBERSHIP_EDIT', 'EDIT', 214, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2054, 'ROLE_EMPLOYEE_MEMBERSHIP_VIEW', 'VIEW', 214, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2055, 'ROLE_EMPLOYEE_MEMBERSHIP_DELETE', 'DELETE', 214, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

    
##Employee Work Shift Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (215, 'Employee Work Shift Details', 'Employee Work Shift Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 215);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2056, 'ROLE_EMPLOYEE_WORK_SHIFT_CREATE', 'CREATE', 215, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2057, 'ROLE_EMPLOYEE_WORK_SHIFT_EDIT', 'EDIT', 215, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2058, 'ROLE_EMPLOYEE_WORK_SHIFT_VIEW', 'VIEW', 215, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2059, 'ROLE_EMPLOYEE_WORK_SHIFT_DELETE', 'DELETE', 215, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);



##Employee Document Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (216, 'Employee Document Details', 'Employee Document Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 216);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2060, 'ROLE_EMPLOYEE_DOCUMENT_CREATE', 'CREATE', 216, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2061, 'ROLE_EMPLOYEE_DOCUMENT_EDIT', 'EDIT', 216, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2062, 'ROLE_EMPLOYEE_DOCUMENT_VIEW', 'VIEW', 216, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2063, 'ROLE_EMPLOYEE_DOCUMENT_DELETE', 'DELETE', 216, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Work Experience Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (217, 'Employee Work Experience Details', 'Employee Work Experience Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 217);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2064, 'ROLE_EMPLOYEE_WORK_EXPERIENCE_CREATE', 'CREATE', 217, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2065, 'ROLE_EMPLOYEE_WORK_EXPERIENCE_EDIT', 'EDIT', 217, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2066, 'ROLE_EMPLOYEE_WORK_EXPERIENCE_VIEW', 'VIEW', 217, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2067, 'ROLE_EMPLOYEE_WORK_EXPERIENCE_DELETE', 'DELETE', 217, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Education Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (218, 'Employee Education Details', 'Employee Education Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 218);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2068, 'ROLE_EMPLOYEE_EDUCATION_CREATE', 'CREATE', 218, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2069, 'ROLE_EMPLOYEE_EDUCATION_EDIT', 'EDIT', 218, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2070, 'ROLE_EMPLOYEE_EDUCATION_VIEW', 'VIEW', 218, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2071, 'ROLE_EMPLOYEE_EDUCATION_DELETE', 'DELETE', 218, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Skill Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (219, 'Employee Skill Details', 'Employee Skill Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 219);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2072, 'ROLE_EMPLOYEE_SKILL_CREATE', 'CREATE', 219, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2073, 'ROLE_EMPLOYEE_SKILL_EDIT', 'EDIT', 219, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2074, 'ROLE_EMPLOYEE_SKILL_VIEW', 'VIEW', 219, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2075, 'ROLE_EMPLOYEE_SKILL_DELETE', 'DELETE', 219, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);



##Employee License Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (220, 'Employee License Details', 'Employee License Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 220);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2076, 'ROLE_EMPLOYEE_LICENSE_CREATE', 'CREATE', 220, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2077, 'ROLE_EMPLOYEE_LICENSE_EDIT', 'EDIT', 220, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2078, 'ROLE_EMPLOYEE_LICENSE_VIEW', 'VIEW', 220, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2079, 'ROLE_EMPLOYEE_LICENSE_DELETE', 'DELETE', 220, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Certification Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (221, 'Employee Certification Details', 'Employee Certification Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 221);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2080, 'ROLE_EMPLOYEE_CERTIFICATION_CREATE', 'CREATE', 221, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2081, 'ROLE_EMPLOYEE_CERTIFICATION_EDIT', 'EDIT', 221, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2082, 'ROLE_EMPLOYEE_CERTIFICATION_VIEW', 'VIEW', 221, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2083, 'ROLE_EMPLOYEE_CERTIFICATION_DELETE', 'DELETE', 221, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Employee Language Details
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (222, 'Employee Language Details', 'Employee Language Details', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 222);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2084, 'ROLE_EMPLOYEE_LANGUAGE_CREATE', 'CREATE', 222, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2085, 'ROLE_EMPLOYEE_LANGUAGE_EDIT', 'EDIT', 222, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2086, 'ROLE_EMPLOYEE_LANGUAGE_VIEW', 'VIEW', 222, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2087, 'ROLE_EMPLOYEE_LANGUAGE_DELETE', 'DELETE', 222, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Pay Grade
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (223, 'Pay Grade', 'Employee Pay Grade', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 223);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2088, 'ROLE_PAY_GRADE_CREATE', 'CREATE', 223, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2089, 'ROLE_PAY_GRADE_EDIT', 'EDIT', 223, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2090, 'ROLE_PAY_GRADE_VIEW', 'VIEW', 223, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2091, 'ROLE_PAY_GRADE_DELETE', 'DELETE', 223, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Salary Category
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (224, 'Salary Category', 'Salary Category', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 224);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2092, 'ROLE_SALARY_CATEGORY_CREATE', 'CREATE', 224, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2093, 'ROLE_SALARY_CATEGORY_EDIT', 'EDIT', 224, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2094, 'ROLE_SALARY_CATEGORY_VIEW', 'VIEW', 224, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2095, 'ROLE_SALARY_CATEGORY_DELETE', 'DELETE', 224, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Salary Component
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (225, 'Salary Component', 'Salary Component', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 225);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2096, 'ROLE_SALARY_STRUCTURE_CREATE', 'CREATE', 225, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2097, 'ROLE_SALARY_STRUCTURE_EDIT', 'EDIT', 225, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2098, 'ROLE_SALARY_STRUCTURE_VIEW', 'VIEW', 225, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2099, 'ROLE_SALARY_STRUCTURE_DELETE', 'DELETE', 225, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Income Tax Bracket
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (226, 'Income Tax Bracket', 'Income Tax Bracket', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 226);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2100, 'ROLE_INCOME_TAX_BRACKET_CREATE', 'CREATE', 226, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2101, 'ROLE_INCOME_TAX_BRACKET_EDIT', 'EDIT', 226, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2102, 'ROLE_INCOME_TAX_BRACKET_VIEW', 'VIEW', 226, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2103, 'ROLE_INCOME_TAX_BRACKET_DELETE', 'DELETE', 226, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Job Title
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (227, 'Job Title', 'Job Title', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 227);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2104, 'ROLE_JOB_TITLE_CREATE', 'CREATE', 227, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2105, 'ROLE_JOB_TITLE_EDIT', 'EDIT', 227, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2106, 'ROLE_JOB_TITLE_VIEW', 'VIEW', 227, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2107, 'ROLE_JOB_TITLE_DELETE', 'DELETE', 227, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Job Category
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (228, 'Job Category', 'Job Category', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 228);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2108, 'ROLE_JOB_CATEGORY_CREATE', 'CREATE', 228, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2109, 'ROLE_JOB_CATEGORY_EDIT', 'EDIT', 228, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2110, 'ROLE_JOB_CATEGORY_VIEW', 'VIEW', 228, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2111, 'ROLE_JOB_CATEGORY_DELETE', 'DELETE', 228, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Job Certification
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (229, 'Job Certification', 'Job Certification', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 229);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2112, 'ROLE_JOB_CERTICATION_CREATE', 'CREATE', 229, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2113, 'ROLE_JOB_CERTICATION_EDIT', 'EDIT', 229, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2114, 'ROLE_JOB_CERTICATION_VIEW', 'VIEW', 229, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2115, 'ROLE_JOB_CERTICATION_DELETE', 'DELETE', 229, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

        

##Job Education
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (230, 'Job Education', 'Job Education', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 230);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2116, 'ROLE_JOB_EDUCATION_CREATE', 'CREATE', 230, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2117, 'ROLE_JOB_EDUCATION_EDIT', 'EDIT', 230, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2118, 'ROLE_JOB_EDUCATION_VIEW', 'VIEW', 230, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2119, 'ROLE_JOB_EDUCATION_DELETE', 'DELETE', 230, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);
		
		
        
##Job License
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (231, 'Job License', 'Job License', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 231);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2120, 'ROLE_JOB_LICENSE_CREATE', 'CREATE', 231, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2121, 'ROLE_JOB_LICENSE_EDIT', 'EDIT', 231, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2122, 'ROLE_JOB_LICENSE_VIEW', 'VIEW', 231, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2123, 'ROLE_JOB_LICENSE_DELETE', 'DELETE', 231, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Job Language
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (232, 'Job Language', 'Job Language', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 232);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2124, 'ROLE_JOB_LANGUAGE_CREATE', 'CREATE', 232, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2125, 'ROLE_JOB_LANGUAGE_EDIT', 'EDIT', 232, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2126, 'ROLE_JOB_LANGUAGE_VIEW', 'VIEW', 232, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2127, 'ROLE_JOB_LANGUAGE_DELETE', 'DELETE', 232, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Job Membership
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (233, 'Job Membership', 'Job Membership', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 233);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2128, 'ROLE_JOB_MEMBERSHIP_CREATE', 'CREATE', 233, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2129, 'ROLE_JOB_MEMBERSHIP_EDIT', 'EDIT', 233, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2130, 'ROLE_JOB_MEMBERSHIP_VIEW', 'VIEW', 233, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2131, 'ROLE_JOB_MEMBERSHIP_DELETE', 'DELETE', 233, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

        
##Job Membership Category
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (234, 'Job Membership Category', 'Job Membership Category', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 234);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2132, 'ROLE_JOB_MEMBERSHIP_CATEGORY_CREATE', 'CREATE', 234, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2133, 'ROLE_JOB_MEMBERSHIP_CATEGORY_EDIT', 'EDIT', 234, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2134, 'ROLE_JOB_MEMBERSHIP_CATEGORY_VIEW', 'VIEW', 234, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2135, 'ROLE_JOB_MEMBERSHIP_CATEGORY_DELETE', 'DELETE', 234, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

        
##Job Skill
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (235, 'Job Skill', 'Job Skill', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 235);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2136, 'ROLE_JOB_SKILL_CREATE', 'CREATE', 235, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2137, 'ROLE_JOB_SKILL_EDIT', 'EDIT', 235, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2138, 'ROLE_JOB_SKILL_VIEW', 'VIEW', 235, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2139, 'ROLE_JOB_SKILL_DELETE', 'DELETE', 235, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

                
##Salary Structure
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (236, 'Salary Structure', 'Salary Structure', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 236);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2140, 'ROLE_SALARY_STRUCTURE_CREATE', 'CREATE', 236, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2141, 'ROLE_SALARY_STRUCTURE_EDIT', 'EDIT', 236, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2142, 'ROLE_SALARY_STRUCTURE_VIEW', 'VIEW', 236, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2143, 'ROLE_SALARY_STRUCTURE_DELETE', 'DELETE', 236, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Employee Salary Structure
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (237, 'Salary Structure Employee', 'Salary Structure Employee', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 237);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2144, 'ROLE_ASSIGN_EMPLOYEE_TO_SALARY_STRUCTURE', 'ASSIGN EMPLOYEE TO SALARY STRUCTURE', 237, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2145, 'ROLE_EMPLOYEE_SALARY_STRUCTURE_DELETE', 'DELETE EMPLOYEE SALARY STRUCTURE', 237, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);

##Salary Component
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (238, 'Salary Structure', 'Salary Structure', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 238);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2146, 'ROLE_SALARY_COMPONENT_CREATE', 'CREATE', 238, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2147, 'ROLE_SALARY_COMPONENT_EDIT', 'EDIT', 238, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2148, 'ROLE_SALARY_COMPONENT_VIEW', 'VIEW', 238, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2149, 'ROLE_SALARY_COMPONENT_DELETE', 'DELETE', 238, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Employee Salary Component
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (239, 'Salary Component Employee', 'Salary Component Employee', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 239);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2150, 'ROLE_ASSIGN_EMPLOYEE_TO_SALARY_COMPONENT', 'ASSIGN EMPLOYEE TO SALARY COMPONENT', 239, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2151, 'ROLE_EMPLOYEE_SALARY_COMPONENT_DELETE', 'DELETE EMPLOYEE SALARY COMPONENT', 239, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);


##Payroll Entry
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (240, 'Payroll Entry', 'Payroll Entry', 31, 'josephkiwia', '2023-11-05 00:00:00', 1, 240);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2152, 'ROLE_PAYROLL_ENTRY_CREATE', 'CREATE', 240, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2153, 'ROLE_PAYROLL_ENTRY_EDIT', 'EDIT', 240, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2154, 'ROLE_PAYROLL_ENTRY_VIEW', 'VIEW', 240, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2155, 'ROLE_PAYROLL_ENTRY_DELETE', 'DELETE', 240, 'josephkiwia', '2023-11-05 00:00:00', 1, 0),
        (2156, 'ROLE_PAYROLL_ENTRY_POST', 'POST', 240, 'josephkiwia', '2023-11-05 00:00:00', 1, 0);



##Expess Claims
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (241, 'Expense Claims', 'Expense Claims', 31, 'jeremi16us', '2024-10-01 00:00:00', 1, 241);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2157, 'ROLE_EXPENSE_CLAIMS_VIEW', 'VIEW', 241, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2158, 'ROLE_EXPENSE_CLAIMS_CREATE', 'CREATE', 241, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2159, 'ROLE_EXPENSE_CLAIMS_EDIT', 'EDIT', 241, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2160, 'ROLE_EXPENSE_CLAIMS_DELETE', 'DELETE', 241, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2161, 'ROLE_EXPENSE_CLAIMS_CANCEL', 'CANCEL', 241, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2162, 'ROLE_EXPENSE_CLAIMS_TRANSFER', 'TRANSFER', 241, 'jeremi16us', '2024-10-01 00:00:00', 1, 0);

##Expess Claims
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (242, 'Expense Claims Type', 'Expense Claims Type', 31, 'jeremi16us', '2024-10-01 00:00:00', 1, 242);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2163, 'ROLE_EXPENSE_CLAIMS_TYPE_VIEW', 'VIEW', 242, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2164, 'ROLE_EXPENSE_CLAIMS_TYPE_CREATE', 'CREATE', 242, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2165, 'ROLE_EXPENSE_CLAIMS_TYPE_EDIT', 'EDIT', 242, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2166, 'ROLE_EXPENSE_CLAIMS_TYPE_DELETE', 'DELETE', 242, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2167, 'ROLE_EXPENSE_CLAIMS_TYPE_CANCEL', 'CANCEL', 242, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (2168, 'ROLE_EXPENSE_CLAIMS_TYPE_TRANSFER', 'TRANSFER', 242, 'jeremi16us', '2024-10-01 00:00:00', 1, 0);

##Asset Status
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (281, 'Asset Status', 'Asset Status', 32, 'josephkiwia', '2024-05-05 00:00:00', 1, 281);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2901, 'ROLE_ASSET_STATUS_CREATE', 'CREATE', 281, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2902, 'ROLE_ASSET_STATUS_EDIT', 'EDIT', 281, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2903, 'ROLE_ASSET_STATUS_VIEW', 'VIEW', 281, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2904, 'ROLE_ASSET_STATUS_DELETE', 'DELETE', 281, 'josephkiwia', '2024-05-05 00:00:00', 1, 0);

##Asset Category
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (282, 'Asset Category', 'Asset Category', 32, 'josephkiwia', '2024-05-05 00:00:00', 1, 282);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2911, 'ROLE_ASSET_CATEGORY_CREATE', 'CREATE', 282, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2912, 'ROLE_ASSET_CATEGORY_EDIT', 'EDIT', 282, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2913, 'ROLE_ASSET_CATEGORY_VIEW', 'VIEW', 282, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2914, 'ROLE_ASSET_CATEGORY_DELETE', 'DELETE', 282, 'josephkiwia', '2024-05-05 00:00:00', 1, 0);

##Asset
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (283, 'Asset', 'Asset', 32, 'josephkiwia', '2024-05-05 00:00:00', 1, 283);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (2921, 'ROLE_ASSET_MANAGEMENT', 'ASSET MANAGEMENT', 283, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2922, 'ROLE_ASSET_CREATE', 'CREATE', 283, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2923, 'ROLE_ASSET_EDIT', 'EDIT', 283, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2924, 'ROLE_ASSET_VIEW', 'VIEW', 283, 'josephkiwia', '2024-05-05 00:00:00', 1, 0),
        (2925, 'ROLE_ASSET_DELETE', 'DELETE', 283, 'josephkiwia', '2024-05-05 00:00:00', 1, 0);


##Form Builder - Form
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (300, 'Form Builder - Form', 'Form Builder - Form', 2, 'jeremi16us', '2023-11-28 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (3001, 'ROLE_FORM_BUILDER_FORM_VIEW', 'VIEW', 300, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3002, 'ROLE_FORM_BUILDER_FORM_CREATE', 'CREATE', 300, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3003, 'ROLE_FORM_BUILDER_FORM_EDIT', 'EDIT', 300, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3004, 'ROLE_FORM_BUILDER_FORM_DELETE', 'DELETE', 300, 'jeremi16us', '2023-11-28 00:00:00', 1, 0);

##Form Builder - Form Field
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (301, 'Form Builder - Form Field', 'Form Builder - Form Field', 2, 'jeremi16us', '2023-11-28 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (3011, 'ROLE_FORM_BUILDER_FORM_FIELD_VIEW', 'VIEW', 301, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3012, 'ROLE_FORM_BUILDER_FORM_FIELD_CREATE', 'CREATE', 301, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3013, 'ROLE_FORM_BUILDER_FORM_FIELD_EDIT', 'EDIT', 301, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3014, 'ROLE_FORM_BUILDER_FORM_FIELD_DELETE', 'DELETE', 301, 'jeremi16us', '2023-11-28 00:00:00', 1, 0);

##Form Builder - Form Field Group
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (302, 'Form Builder - Form Field Group', 'Form Builder - Form Field Group', 2, 'jeremi16us', '2023-11-28 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (3021, 'ROLE_FORM_BUILDER_FORM_FIELD_GROUP_VIEW', 'VIEW', 302, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3022, 'ROLE_FORM_BUILDER_FORM_FIELD_GROUP_CREATE', 'CREATE', 302, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3023, 'ROLE_FORM_BUILDER_FORM_FIELD_GROUP_EDIT', 'EDIT', 302, 'jeremi16us', '2023-11-28 00:00:00', 1, 0),
        (3024, 'ROLE_FORM_BUILDER_FORM_FIELD_GROUP_DELETE', 'DELETE', 302, 'jeremi16us', '2023-11-28 00:00:00', 1, 0);

##Form Builder - Form Record
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (303, 'Form Builder - Form Record', 'Form Builder - Form Record', 2, 'jeremi16us', '2023-12-12 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (3031, 'ROLE_FORM_BUILDER_FORM_RECORD_VIEW', 'VIEW', 303, 'jeremi16us', '2023-12-12 00:00:00', 1, 0),
        (3032, 'ROLE_FORM_BUILDER_FORM_RECORD_CREATE', 'CREATE', 303, 'jeremi16us', '2023-12-12 00:00:00', 1, 0),
        (3033, 'ROLE_FORM_BUILDER_FORM_RECORD_EDIT', 'EDIT', 303, 'jeremi16us', '2023-12-12 00:00:00', 1, 0),
        (3034, 'ROLE_FORM_BUILDER_FORM_RECORD_DELETE', 'DELETE', 303, 'jeremi16us', '2023-12-12 00:00:00', 1, 0);

##Patient Transfer Request
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (305, 'Patient Transfer Request', 'Patient Transfer Request', 16, 'jeremi16us', '2024-10-01 00:00:00', 1, 8);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (3051, 'ROLE_PATIENT_TRANSFER_REQUEST_VIEW', 'VIEW', 305, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (3052, 'ROLE_PATIENT_TRANSFER_REQUEST_CREATE', 'CREATE', 305, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (3053, 'ROLE_PATIENT_TRANSFER_REQUEST_EDIT', 'EDIT', 305, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (3054, 'ROLE_PATIENT_TRANSFER_REQUEST_DELETE', 'DELETE', 305, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (3055, 'ROLE_PATIENT_TRANSFER_REQUEST_CANCEL', 'CANCEL', 305, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (3056, 'ROLE_PATIENT_TRANSFER_REQUEST_TRANSFER', 'TRANSFER', 305, 'jeremi16us', '2024-10-01 00:00:00', 1, 0);


##Bulksms 
INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES  (401, 'Bulksms', 'Bulksms', 33, 'jeremi16us', '2024-10-01 00:00:00', 1, 401);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (4011, 'ROLE_BULKSMS_VIEW', 'VIEW', 401, 'jeremi16us', '2024-10-01 00:00:00', 1, 0),
        (4012, 'ROLE_BULKSMS_TOPUP', 'TOPUP', 401, 'jeremi16us', '2024-10-01 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES 
        (402, 'Doctor Admission Details', 'Allow Doctor To Add Admission Details', 11, 'Imani Mwambelo', '2024-12-27 00:00:00', 1, 2),
        (403, 'Edit Doctor Admission Details', 'Allow Nurse To Edit Doctor Admission Details', 10, 'Imani Mwambelo', '2024-12-27 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES 
        (4013, 'ROLE_DOCTOR_ADD_ADMISSION_DETAILS', 'Adding ward and bed for patients', 402, 'Imani Mwambelo', '2024-12-27 00:00:00', 1, 0),
        (4014, 'ROLE_EDIT_DOCTOR_ADMISSION_DETAILS', 'Edit ward and bed selected by doctor', 403, 'Imani Mwambelo', '2024-12-27 00:00:00', 1, 0);

INSERT INTO tbl_permission_group (id, name, description, permission_block_id, created_by, created_time, active, sort)
VALUES (404, 'Patient Vital', 'Patient Vital', 10, 'shedrack', '2024-12-17 00:00:00', 1, 2);

INSERT INTO tbl_permission (id, name, description, permission_group_id, created_by, created_time, active, sort)
VALUES  (4015, 'ROLE_PATIENT_VITAL_VIEW', 'VIEW', 404, 'shedrack', '2024-12-17 00:00:00', 1, 0),
        (4016, 'ROLE_PATIENT_VITAL_EDIT', 'EDIT', 404, 'shedrack', '2024-12-17 00:00:00', 1, 0),
        (4017, 'ROLE_PATIENT_VITAL_CREATE', 'CREATE', 404, 'shedrack', '2024-12-17 00:00:00', 1, 0),
        (4018, 'ROLE_PATIENT_VITAL_DELETE', 'DELETE', 404, 'shedrack', '2024-12-17 00:00:00', 1, 0);
